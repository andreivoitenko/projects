Author: Andrei Voitenko


Folders:

    - JAVA:
        - algoritmid - homeworks
        - BioCC-App - web application for BioCC (in progress)
        - Blackjack - blackjack game
        - station - Android application for finding free parking spots for vehicles
        
    - Python-project - blackjack game
    
    - Voitenko_Informaatika_2018 - my Bachelor degree thesis

    - haskell - homeworks
    
    - second - Unity shooter game