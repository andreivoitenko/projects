package sample;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.canvas.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.scene.Group;
import java.util.Random;

class Sprite {//kaardi liikumise kiirus
    double x = -1000, y = -1000, tx = 0, ty = 0, pick_timer = 0;
    boolean picking = false;
    byte id = 0;

    void trg_mov(double speedX, double speedY, double maxX, double maxY)
    {
        double sx = (tx - x) / speedX;
        double sy = (ty - y) / speedY;
        x += sx >= 0 ? sx > maxX ? maxX : sx : sx < -maxX ? -maxX : sx;
        y += sy >= 0 ? sy > maxY ? maxY : sy : sy < -maxY ? -maxY : sy;

        /*if(sx >= 0) {
            if(sx > maxX)
                realspeedX = maxX;
            else
                realspeedX = sx;
        }
        else {
            if(sx < -maxX)
                realspeedX = -maxX;
            else
                realspeedX = sx;
        }
        */
    }
}

class Card extends Sprite {//vottab kaardi pildist
    static Image grid;
    static final int
            xstep = 73, ystep = 98, width = 72,
            height = 96, xoffset = 1, yoffset = 1;

    byte suit = 0, own = 0;
}

class Chip extends Sprite {
    static Image grid;
    static Image grid_dark;
    static final int d = 61, r = 30, istep = 123;
}

public class Main extends Application {//muutujad
    static GraphicsContext gc;
    static Canvas canvas;

    static Card[] cards;
    static int cardindex = 0;

    static Chip[] chips;
    static int[] mchips_count;

    static boolean playing = false;
    static boolean can_split = false;
    static boolean can_double = true;
    static boolean splitted = false;
    static boolean can_bet = true;
    static boolean want_start = false;
    static boolean split_next = false;

    static final double deck_x = 100.0, deck_y = 100.0;
    static Random rand = new Random();

    static final int p1h_x = 250, p1h_y = 300;
    static final int p2h_x = 100, p2h_y = 300;
    static final int ap2h_x = 150, ap2h_y = 300;
    static final int dh_x = 250, dh_y = 100;

    static final int cs_x = 650, cs_y = 500;
    static final int cs_ox = -15;

    static final int coff_x = 17;
    static final int coff_y = 10;

    static int d_index = 0;

    static int p_bet = 0;
    static int p_money = 180;
    static int p_index1 = 0;
    static int p_index2 = 0;

    static int button_radius = 40;
    static int[] buttons;

    static Image deck_image;
    static Image buttons_image;
    static Image chip_stack_img;

    static double card_pick_timer = 0.0;
    static double card_pick_latency = 4.0;
    void move_card(int id, int x, int y) {
        cards[id].tx = x;
        cards[id].ty = y;
        cards[id].picking = true;
        cards[id].pick_timer = card_pick_timer;
        card_pick_timer += card_pick_latency;
    }

    void p_give()//annab kaardi
    {
        if(splitted) {
            cards[cardindex].own = split_next ? (byte) 1 : (byte) 2;
            if (split_next)
                move_card(cardindex++, p1h_x + p_index1 * coff_x, p1h_y + p_index1++ * coff_y);
            else
                move_card(cardindex++, p2h_x + p_index2 * coff_x, p2h_y + p_index2++ * coff_y);
        }
        else {
            cards[cardindex].own = (byte) 1;
            move_card(cardindex++, p1h_x + p_index1 * coff_x, p1h_y + p_index1++ * coff_y);
        }
    }

    void d_give()//annab kaardi dealer
    {
        cards[cardindex].own = 3;
        move_card(cardindex++, dh_x + d_index * coff_x, dh_y - d_index++ * coff_y);
    }

    int get_score(int p)
    {
        int sc = 0;
        int a = 0;
        for(Card c : cards)
            if(c.own == p) {
                if (c.id == 0)
                    a++;
                sc += c.id < 9 ? c.id + 1 : 10;
            }
        for(int x = 0; x < a; x++) {
            if(sc + 10 <= 21)
                sc += 10;
        }
        return sc;
    }

    void hold()//hold
    {
        playing = false;
        can_double = false;
        can_bet = true;
        splitted = false;
        split_next = false;

        while(get_score(3) < 17)//dealer vottab kaardi
            d_give();

        int sc_d = get_score(3);
        int sc_p = get_score(1);
        int sc_p2 = get_score(2);
        if(sc_d > 21)
            sc_d = -1;

        if(sc_p > 21)
            sc_p = -1;

        if(sc_p2 > 21)
            sc_p2 = -1;

        if(sc_p == -1 && !splitted){
            p_money -= p_bet;
            p_bet = 0;
            split_chips();
            return;
        }

        if(splitted && sc_p2 <= sc_d) {
            p_bet /= 2;
            p_money -= p_bet;
        }

        if(sc_d <= sc_p) {
            if(sc_p > sc_d)
                p_money += p_bet;
            for(int x = 4; x < chips.length; x++) {
                chips[x].x = chips[3].x;
                chips[x].y = chips[3].y;
            }

            chip_pick_timer = 5.0;

            split_chips();//raha eraldumie
            int gived_money = 0;
            while (gived_money < (sc_p == sc_d ? p_bet : p_bet * 2)) {
                int next = rand.nextInt(3);
                if (mchips_count[next] > 0) {
                    if (next == 0)
                        gived_money += 5;
                    else if (next == 1)
                        gived_money += 10;
                    else if (next == 2)
                        gived_money += 20;

                    Chip c = chips[chip_index];
                    c.id = chips[next].id;
                    c.tx = chips[next].x;
                    c.ty = chips[next].y;
                    c.picking = true;
                    c.pick_timer = chip_pick_timer;
                    chip_pick_timer += chip_pick_latency;

                    if (++chip_index >= chips.length)
                        chip_index = 4;
                }
            }
        }
        else if(sc_d > sc_p) {
            p_money -= p_bet;
            split_chips();
        }
        p_bet = 0;

    }

    static double chip_pick_timer = 0.0;
    static final double chip_pick_latency = 2.0;
    static int chip_index = 4;
    void move_chip(int id, int x, int y) {
        Chip c = chips[chip_index];
        c.x = chips[id].x;
        c.y = chips[id].y;
        c.tx = x;
        c.ty = y;
        c.id = chips[id].id;
        c.picking = true;
        c.pick_timer = chip_pick_timer;
        chip_pick_timer += chip_pick_latency;

        if(++chip_index >= chips.length)
            chip_index = 4;
    }

    void split_chips()
    {
        mchips_count[1] = 0;
        mchips_count[2] = 0;

        if(p_money - 30 >= 0)
        {
            mchips_count[0] = p_money % 10 == 5 ? 7 : 6;
            if(p_money - 110 >= 0)
            {
                mchips_count[1] = 8;
                mchips_count[2] = (p_money - 110) / 20;
            }
            else
            {
                mchips_count[1] = (p_money - 30) / 10;
            }
        }
        else
        {
            mchips_count[0] = p_money / 5;
        }
    }

    @Override public void start(Stage stage) {
        Card.grid = new Image("cards.png");
        Chip.grid = new Image("chips.png");
        Chip.grid_dark = new Image("chips_dark.png");
        deck_image = new Image("ka.png");
        buttons_image = new Image("buttons.png");
        chip_stack_img = new Image("fi.png");
        Image background = new Image("stol3.png");
        ImageView bg = new ImageView(background);
        bg.setX(0);
        bg.setY(0);


        Group root = new Group();
        canvas = new Canvas(800, 600);
        gc = canvas.getGraphicsContext2D();
        root.getChildren().add(bg);
        root.getChildren().add(canvas);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.sizeToScene();
        stage.show();

        cards = new Card[52];
        for(int x = 0; x < cards.length; x++)
            cards[x] = new Card();


        for(byte x = 0; x < cards.length; x++)
        {
            cards[x].own = 0;
            cards[x].suit = (byte)(x / 13);
            cards[x].id = (byte)(x % 13);   //matemaatika
            cards[x].x = deck_x;
            cards[x].y = deck_y;
            cards[x].tx = deck_x;
            cards[x].ty = deck_y;
        }

        for(byte x = 0; x < cards.length; x++)
        {
            int new_index = rand.nextInt(cards.length);//shuffle kolodu
            Card temp = cards[x];
            cards[x] = cards[new_index];
            cards[new_index] = temp;
        }
        //END : deck init

        chips = new Chip[32];   // amount of chips ~~ on board
        mchips_count = new int[3];
        for(int x = 0; x < chips.length; x++)
            chips[x] = new Chip();

        //chips asukoht
        chips[0].id = (byte)0;
        chips[0].x = 500.0;
        chips[0].y = 350.0;

        chips[1].id = (byte)1;
        chips[1].x = 550.0;
        chips[1].y = 350.0;

        chips[2].id = (byte)2;
        chips[2].x = 600.0;
        chips[2].y = 350.0;

        chips[3].id = (byte)rand.nextInt(3);
        chips[3].x = 550.0;
        chips[3].y = 100.0;
        //END : chips init

        buttons = new int[10];
        buttons[0] = 100;
        buttons[1] = 450;
        buttons[2] = 200;
        buttons[3] = 465;
        buttons[4] = 300;
        buttons[5] = 475;
        buttons[6] = 400;
        buttons[7] = 470;
        buttons[8] = 250;
        buttons[9] = 475;

        split_chips();

        //freimide arv sekundis
        Timeline renTimer = new Timeline
                (new KeyFrame(javafx.util.Duration.millis(8), render -> {
                    if(chip_pick_timer > 0)
                        chip_pick_timer -= 0.1;
                    if(card_pick_timer > 0)
                        card_pick_timer -= 0.1;

                    gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());//delete all

                    boolean can_start = true;

                    for(Card s : cards) {
                        if(s.picking && (s.pick_timer -= 0.1) <= 0.0) {
                            s.trg_mov(25.0, 25.0, 3.0, 3.0);//kaardi liikumine
                            if(Math.abs(s.x - s.tx) <= 1.0 && Math.abs(s.y - s.ty) <= 1.0) {
                                s.picking = false;
                                s.x = s.tx;
                                s.y = s.ty;
                            }
                        }
                        Main.gc.drawImage(Card.grid, s.id * Card.xstep + Card.xoffset, s.suit * Card.ystep + Card.yoffset,
                                Card.width, Card.height, (int)s.x, (int)s.y, Card.width, Card.height);

                        if(s.picking)
                            can_start = false;
                    }

                    if(want_start && can_start) {
                        want_start = false;

                        for(byte x = 0; x < cards.length; x++)
                        {
                            int new_index = rand.nextInt(cards.length);//random
                            Card temp = cards[x];
                            cards[x] = cards[new_index];
                            cards[new_index] = temp;
                            cards[x].x = deck_x;
                            cards[x].y = deck_y;
                            cards[x].tx = deck_x;
                            cards[x].ty = deck_y;
                            cards[x].picking = false;
                        }
                        //cards[0].id = cards[1].id; //DBG    / dlja postojannogo splita

                        p_give();//esimesed 2 kaardi
                        p_give();

                        if((cards[0].id == cards[1].id) || (cards[0].id > 9 && cards[1].id > 9))
                            can_split = true;

                        d_give();
                        playing = true;
                        can_double = true;
                    }

                    Chip s = chips[0];
                    Main.gc.drawImage(mchips_count[0] > 0 ? Chip.grid : Chip.grid_dark, s.id * Chip.istep, 0, Chip.istep, Chip.istep,
                            (int)s.x, (int)s.y, Chip.d, Chip.d);
                    s = chips[1];
                    Main.gc.drawImage(mchips_count[1] > 0 ? Chip.grid : Chip.grid_dark, s.id * Chip.istep, 0, Chip.istep, Chip.istep,
                            (int)s.x, (int)s.y, Chip.d, Chip.d);
                    s = chips[2];
                    Main.gc.drawImage(mchips_count[2] > 0 ? Chip.grid : Chip.grid_dark, s.id * Chip.istep, 0, Chip.istep, Chip.istep,
                            (int)s.x, (int)s.y, Chip.d, Chip.d);
                    s = chips[3];
                    Main.gc.drawImage(Chip.grid, s.id * Chip.istep, 0, Chip.istep, Chip.istep,
                            (int)s.x, (int)s.y, Chip.d, Chip.d);

                    for(int x = 3; x < chips.length; x++) {//chips liikumine
                        boolean mark_for_delete = false;
                        s = chips[x];
                        if(s.picking && (s.pick_timer -= 0.1) <= 0.0) {
                            s.trg_mov(10.0, 5.0, 3.0, 3.0);
                            if (Math.abs(s.x - s.tx) <= 1.0 && Math.abs(s.y - s.ty) <= 1.0) {
                                s.x = s.tx;
                                s.y = s.ty;
                                if(s.x == chips[3].x && s.y == chips[3].y)
                                    chips[3].id = s.id;
                                mark_for_delete = true;
                            }
                        }
                        if(s.picking) {
                            Main.gc.drawImage(Chip.grid,
                                    s.id * Chip.istep, 0, Chip.istep, Chip.istep, (int)s.x, (int)s.y, Chip.d, Chip.d);
                            if(mark_for_delete)
                                s.picking = false;
                        }
                    }

                    if(playing) {//game pictures
                        for (int x = 0; x < 4; x += 2) {
                            int p = (x + 1) / 2;
                            gc.drawImage(buttons_image, p * 123, 0, 123, 123,
                                    buttons[x], buttons[x + 1], button_radius * 2, button_radius * 2);
                        }

                        if(can_double && p_bet * 2 <= p_money)
                            gc.drawImage(buttons_image, 2 * 123, 0, 123, 123,
                                    buttons[4], buttons[5], button_radius * 2, button_radius * 2);

                        if(can_split && p_bet * 2 <= p_money)
                            gc.drawImage(buttons_image, 3 * 123, 0, 123, 123,
                                    buttons[6], buttons[7], button_radius * 2, button_radius * 2);
                    }
                    else if(p_bet > 0)
                        gc.drawImage(buttons_image, 4 * 123, 0, 123, 123,
                                buttons[8], buttons[9], button_radius * 2, button_radius * 2);

                    gc.drawImage(deck_image, deck_x, deck_y);

                    for(int x = 0; x < (p_money - p_bet) / 20; x++) {
                        gc.drawImage(chip_stack_img, cs_x + x * cs_ox, cs_y);
                    }
                }));
        //END : update, render

        renTimer.setCycleCount(Animation.INDEFINITE);
        renTimer.play();

        root.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                double x = event.getSceneX();
                double y = event.getSceneY();

                if(!playing) {
                    if(can_bet) {
                        if (mchips_count[2] > 0 && circle_collide(x, y, 0, chips[2].x + Chip.r, chips[2].y + Chip.r, Chip.r)) {
                            p_bet += 20;
                            mchips_count[2]--;
                            move_chip(2, (int) chips[3].x, (int) chips[3].y);
                        } else if (mchips_count[1] > 0 && circle_collide(x, y, 0, chips[1].x + Chip.r, chips[1].y + Chip.r, Chip.r)) {
                            p_bet += 10;
                            mchips_count[1]--;
                            move_chip(1, (int) chips[3].x, (int) chips[3].y);
                        } else if (mchips_count[0] > 0 && circle_collide(x, y, 0, chips[0].x + Chip.r, chips[0].y + Chip.r, Chip.r)) {
                            p_bet += 5;
                            mchips_count[0]--;
                            move_chip(0, (int) chips[3].x, (int) chips[3].y);
                        }
                    }
                    if (!want_start && p_bet > 0 && circle_collide(x, y, 0, buttons[8] + button_radius, buttons[9] + button_radius, button_radius)) {//kopka hit
                        p_index1 = 0;
                        p_index2 = 0;
                        d_index = 0;
                        cardindex = 0;
                        card_pick_timer = 0.0;
                        card_pick_latency = 1.0;
                        for(int b = 0; b < cards.length; b++) {
                            cards[b].own = 0;
                            if((int)cards[b].x != (int)deck_x || (int)cards[b].y != (int)deck_x)
                                move_card(b, (int)deck_x, (int)deck_y);
                        }
                        card_pick_latency = 4.0;
                        want_start = true;
                    }
                }
                else {//vottab kaardi
                    if (circle_collide(x, y, 0, buttons[0] + button_radius, buttons[1] + button_radius, button_radius)) {
                        p_give();
                        can_split = false;
                        can_double = false;

                        if(!splitted) {
                            if (get_score(1) > 21)
                                hold();
                        } else {
                            if(split_next && get_score(1) > 21)
                                hold();
                            else if (!split_next && get_score(2) > 21) {
                                split_next = true;
                                p_index2 = 0;
                                card_pick_latency = 0.3;
                                card_pick_timer = 0.0;
                                for(int z = 0; z < cards.length; z++) {
                                    if(cards[z].own == 2)
                                        move_card(z, ap2h_x + p_index2 * coff_x, ap2h_y + p_index2++ * coff_y);
                                }
                                card_pick_latency = 4.0;
                            }
                        }
                    }
                    else
                    if(p_money >= p_bet * 2 && can_double && circle_collide(x, y, 0, buttons[4] + button_radius, buttons[5] + button_radius, button_radius)) {//knopka double
                        int trgbet = p_bet * 2;
                        int w_bet = p_bet;
                        can_split = false;
                        while (w_bet < trgbet || (mchips_count[0] == 0 && mchips_count[1] == 0 && mchips_count[2] == 0))
                        {
                            int next = rand.nextInt(3);

                            if((next == 0 || w_bet + 5 == trgbet) && mchips_count[0] > 0) {
                                move_chip(0, (int) chips[3].x, (int) chips[3].y);
                                mchips_count[0]--;
                                w_bet += 5;
                            }
                            else if ((next == 1 || w_bet + 10 == trgbet) && mchips_count[1] > 0) {
                                move_chip(1, (int) chips[3].x, (int) chips[3].y);
                                mchips_count[1]--;
                                w_bet += 10;
                            } else if ((next == 2 || w_bet + 20 == trgbet) && mchips_count[2] > 0) {
                                move_chip(2, (int) chips[3].x, (int) chips[3].y);
                                mchips_count[2]--;
                                w_bet += 20;
                            }
                        }
                        p_bet *= 2;
                        p_give();
                        hold();
                    }
                    else if (circle_collide(x, y, 0, buttons[2] + button_radius, buttons[3] + button_radius, button_radius)) {//berjot kartu dlja double
                        if(splitted) {
                            if (!split_next) {
                                split_next = true;
                                p_index2 = 0;
                                card_pick_latency = 0.3;
                                card_pick_timer = 0.0;
                                for(int z = 0; z < cards.length; z++) {
                                    if(cards[z].own == 2)
                                        move_card(z, ap2h_x + p_index2 * coff_x, ap2h_y + p_index2++ * coff_y);
                                }
                                card_pick_latency = 4.0;
                            }
                            else
                                hold();
                        } else hold();
                    }
                    else if (p_money >= p_bet * 2 && can_split && circle_collide(x, y, 0, buttons[6] + button_radius, buttons[7] + button_radius, button_radius))
                    {//split nupp
                        can_double = false;
                        splitted = true;
                        can_split = false;
                        p_index1--;
                        cards[1].own = 2;
                        move_card(1, p2h_x, p2h_y);
                        p_index2++;
                        int trgbet = p_bet * 2;
                        int w_bet = p_bet;
                        while (w_bet < trgbet || (mchips_count[0] == 0 && mchips_count[1] == 0 && mchips_count[2] == 0))
                        {
                            int next = rand.nextInt(3);

                            if((next == 0 || w_bet + 5 == trgbet) && mchips_count[0] > 0) {
                                move_chip(0, (int) chips[3].x, (int) chips[3].y);
                                mchips_count[0]--;
                                w_bet += 5;
                            }
                            else if ((next == 1 || w_bet + 10 == trgbet) && mchips_count[1] > 0) {
                                move_chip(1, (int) chips[3].x, (int) chips[3].y);
                                mchips_count[1]--;
                                w_bet += 10;
                            } else if ((next == 2 || w_bet + 20 == trgbet) && mchips_count[2] > 0) {
                                move_chip(2, (int) chips[3].x, (int) chips[3].y);
                                mchips_count[2]--;
                                w_bet += 20;
                            }
                        }
                        p_bet *= 2;
                    }
                }
            }
        });
    }

    boolean circle_collide(double x1, double y1, double r1, double x2, double y2, double r2) {//chips asukoht
        double dx = x1 - x2;
        double dy = y1 - y2;
        double dr = r1 + r2;
        return dx * dx + dy * dy <= dr * dr;
    }

    public static void main(String[] args) {//start game
        launch(Main.class);
    }
}