package com.example.biocc.model;

import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;



@Indexed
@Entity
public class Inputs {

    @Id
    @GeneratedValue
    Long id;

    @Field
    private String Acronym;

    @Field
    private String Old_nummeration;

    @Field
    private String Isolation_source;

    @Field
    private String Isolation_date;

    @Field
    private String Genus;

    @Field
    private String Species;

    @Field
    private String Subspecies;

    @Field
    private String MALDI_score;

    @Field
    private String Position_in_fridge;

    @Field
    private String Storage_date;

    @Field
    private String Cultivation_conditions;

    public Inputs(){
    }

    public Inputs(String Acronym, String Old_nummeration, String Isolation_source, String Isolation_date,
                  String Genus, String Species, String Subspecies, String MALDI_score, String Position_in_fridge,
                  String Storage_date, String Cultivation_conditions){
        this.Acronym = Acronym;
        this.Cultivation_conditions = Cultivation_conditions;
        this.Genus = Genus;
        this.Isolation_date = Isolation_date;
        this.Isolation_source = Isolation_source;
        this.MALDI_score = MALDI_score;
        this.Old_nummeration = Old_nummeration;
        this.Position_in_fridge = Position_in_fridge;
        this.Species = Species;
        this.Storage_date = Storage_date;
        this.Subspecies = Subspecies;
    }


    public String getAcronym() {
        return Acronym;
    }

    public void setAcronym(String acronym) {
        Acronym = acronym;
    }

    public String getOld_nummeration() {
        return Old_nummeration;
    }

    public void setOld_nummeration(String old_nummeration) {
        Old_nummeration = old_nummeration;
    }

    public String getIsolation_source() {
        return Isolation_source;
    }

    public void setIsolation_source(String isolation_source) {
        Isolation_source = isolation_source;
    }

    public String getIsolation_date() {
        return Isolation_date;
    }

    public void setIsolation_date(String isolation_date) {
        Isolation_date = isolation_date;
    }

    public String getGenus() {
        return Genus;
    }

    public void setGenus(String genus) {
        Genus = genus;
    }

    public String getSpecies() {
        return Species;
    }

    public void setSpecies(String species) {
        Species = species;
    }

    public String getSubspecies() {
        return Subspecies;
    }

    public void setSubspecies(String subspecies) {
        Subspecies = subspecies;
    }

    public String getMALDI_score() {
        return MALDI_score;
    }

    public void setMALDI_score(String MALDI_score) {
        this.MALDI_score = MALDI_score;
    }

    public String getPosition_in_fridge() {
        return Position_in_fridge;
    }

    public void setPosition_in_fridge(String position_in_fridge) {
        Position_in_fridge = position_in_fridge;
    }

    public String getStorage_date() {
        return Storage_date;
    }

    public void setStorage_date(String storage_date) {
        Storage_date = storage_date;
    }

    public String getCultivation_conditions() {
        return Cultivation_conditions;
    }

    public void setCultivation_conditions(String cultivation_conditions) {
        Cultivation_conditions = cultivation_conditions;
    }

}
