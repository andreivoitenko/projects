package com.example.biocc.Controllers;

import com.example.biocc.model.Inputs;
import com.example.biocc.dao.InputsRepository;
import com.example.biocc.service.HibernateSearchService;
import com.example.biocc.service.Inp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Random;

@Controller
public class AppController {

    @Autowired
    InputsRepository repo;




    @GetMapping
    public String start(Model list){

        list.addAttribute("attributes", repo.findAll());
        return "FirstPage";
    }

    @GetMapping(value="/form")
    public String form(Model list){
        list.addAttribute("form", new Inputs());
        return "AddToDb";
    }

    @PostMapping(value="/form")
    public String create(Inputs input){
        repo.save(input);
        return "redirect:FirstPage";
    }

    @Autowired
    private HibernateSearchService searchservice;



    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public String search(@RequestParam(value = "search", required = false) String q, Model model) {
        List<Inputs> searchResults = null;
        try {


            searchResults = searchservice.fuzzySearch(q);

        } catch (Exception ex) {
            System.out.println(ex);
        }
        model.addAttribute("search", searchResults);
        return "Search";

    }

    @GetMapping("/random")
    public String addRandonm(Model list){

        Inputs DataInp = new Inputs();
        DataInp.setAcronym(getSaltString());
        DataInp.setOld_nummeration(getSaltString());
        DataInp.setIsolation_source(getSaltString());
        DataInp.setIsolation_date(getSaltString());
        DataInp.setGenus(getSaltString());
        DataInp.setSpecies(getSaltString());
        DataInp.setSubspecies(getSaltString());
        DataInp.setMALDI_score(getSaltString());
        DataInp.setPosition_in_fridge(getSaltString());
        DataInp.setStorage_date(getSaltString());
        DataInp.setCultivation_conditions(getSaltString());

        repo.save(DataInp);

        list.addAttribute("attributes", repo.findAll());
        return "FirstPage";
    }

    protected String getSaltString() {
        String SALTCHARS = "qwertyuiopasdfghjklzxcvbnm1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 6) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }


}
