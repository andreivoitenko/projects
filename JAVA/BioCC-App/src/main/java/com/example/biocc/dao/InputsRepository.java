package com.example.biocc.dao;

import org.springframework.data.repository.CrudRepository;

import com.example.biocc.model.Inputs;


public interface InputsRepository extends CrudRepository<Inputs, Long> {


}

