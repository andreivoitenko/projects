import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Andrei on 28-Sep-16.
 */
public class kodutöö2 {
    //tegime koos Aleksander Nikolajeviga


    static ArrayList risttahukas(){
        //siin tehakse kast mille mõõtmed on h, a ja b
        ArrayList<Integer> kast = new ArrayList<>();
        int h = (int) (Math.random()*50+10);
        int a = (int) (Math.random()*50+10);
        int b = (int) (Math.random()*50+10);
        kast.add(h);
        kast.add(a);
        kast.add(b);

        return kast;
    }

    static void controller(ArrayList kastid){
        //kontrollitakse kolm kasti, et esimene oleks koige suurem, teine temast vaiksem ja kolmas koge vaiksem
        int a = 1;
        for(int esimene=0; esimene<kastid.size(); esimene++){
            for(int teine = 0; teine<kastid.size(); teine++){
                for(int kolmas=0; kolmas<kastid.size();kolmas++){
                    if(esimene != teine && esimene != kolmas && teine != kolmas){
                        //esimene(h) > teine(h) > kolmas(h)
                        //esimene(a) > teine(a) > kolmas(a)
                        //esimene(b) > teine(b) > kolmas(b)
                         if(((int)((ArrayList)kastid.get(esimene)).get(0) > (int)((ArrayList)kastid.get(teine)).get(0) &&
                                 (int)((ArrayList)kastid.get(esimene)).get(1) > (int)((ArrayList)kastid.get(teine)).get(1) &&
                                 (int)((ArrayList)kastid.get(esimene)).get(2) > (int)((ArrayList)kastid.get(teine)).get(2)) ||
                                 ((int)((ArrayList)kastid.get(esimene)).get(0) > (int)((ArrayList)kastid.get(teine)).get(0) &&
                                         (int)((ArrayList)kastid.get(esimene)).get(1) > (int)((ArrayList)kastid.get(teine)).get(2) &&
                                         (int)((ArrayList)kastid.get(esimene)).get(2) > (int)((ArrayList)kastid.get(teine)).get(1))){

                             if(((int)((ArrayList)kastid.get(teine)).get(0) > (int)((ArrayList)kastid.get(kolmas)).get(0) &&
                                     (int)((ArrayList)kastid.get(teine)).get(1) > (int)((ArrayList)kastid.get(kolmas)).get(1) &&
                                     (int)((ArrayList)kastid.get(teine)).get(2) > (int)((ArrayList)kastid.get(kolmas)).get(2)) ||
                                     ((int)((ArrayList)kastid.get(teine)).get(0) > (int)((ArrayList)kastid.get(kolmas)).get(0) &&
                                             (int)((ArrayList)kastid.get(teine)).get(1) > (int)((ArrayList)kastid.get(kolmas)).get(2) &&
                                             (int)((ArrayList)kastid.get(teine)).get(2) > (int)((ArrayList)kastid.get(kolmas)).get(1))){

                                 ArrayList<ArrayList> finish = new ArrayList<>();
                                 finish.add((ArrayList) kastid.get(esimene));
                                 finish.add((ArrayList) kastid.get(teine));
                                 finish.add((ArrayList) kastid.get(kolmas));
                                 System.out.println("Kolmik №" + a + ":" + " " + finish.get(0) + " => " + finish.get(1) + " => " + finish.get(2));;
                                 a++;

                             }
                         }
                    }
                }
            }
        }
    }

    public static void main(String[] args){
        //tehakse juhuslik arv kaste ja kutsub controller meetodi
        int risttahukateArv = (int) (Math.random()*50+10);
        ArrayList<ArrayList> kastid = new ArrayList<>();
        System.out.println("Kokku kaste: " + risttahukateArv + " tükki");

        for(;kastid.size() < risttahukateArv; ){
            kastid.add(risttahukas());
        }

        System.out.print("Kõik kastid: ");

        for(int i = 0; i<kastid.size();i++){
            System.out.print(kastid.get(i) + ", ");
        }

        System.out.println();
        System.out.println("Kus esimene arv on kõrgus, teine - a ja kolmas - b");
        System.out.println();
        System.out.println("Kõik kombinatsioonid, kus esimene kast on kõige suurem, teine väiksem ja kolmas kõige väiksem:");
        controller(kastid);

    }
}