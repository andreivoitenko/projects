import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Arrays;


/**
 * Created by Andrei on 17-Sep-16.
 */
public class Kodutoo extends javafx.application.Application {

    // joonistatakse graafik
    @Override public void start(Stage stage) {
        stage.setTitle("Kodutöö");
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("Pikkus");
        yAxis.setLabel("Aeg");
        final LineChart<Number,Number> lineChart =
                new LineChart<Number,Number>(xAxis,yAxis);


        lineChart.setTitle("Ajalise keerukuse empiiriline hindamine");
        int a = (int) (0 + Math.random()*50);

        XYChart.Series series = new XYChart.Series();
        XYChart.Series series2 = new XYChart.Series();
        XYChart.Series series3 = new XYChart.Series();
        XYChart.Series series4 = new XYChart.Series();

        series.setName("Kiir: " + main(a).get(0));
        series2.setName("Aeglane: " + main(a).get(2));
        series3.setName("Kiir juhuslik: " + main(a).get(4));
        series4.setName("Aeglane juhuslik: " + main(a).get(6));

        series.getData().add(new XYChart.Data(0, 0));
        series.getData().add(new XYChart.Data(main(a).get(1), main(a).get(0)));

        series2.getData().add(new XYChart.Data(0, 0));
        series2.getData().add(new XYChart.Data(main(a).get(3), main(a).get(2)));

        series3.getData().add(new XYChart.Data(0, 0));
        series3.getData().add(new XYChart.Data(main(a).get(5), main(a).get(4)));

        series4.getData().add(new XYChart.Data(0, 0));
        series4.getData().add(new XYChart.Data(main(a).get(7), main(a).get(6)));

        Scene scene  = new Scene(lineChart,800,600);
        lineChart.getData().addAll(series, series2, series3, series4);

        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    //kiirmeetod
    static void kiir(ArrayList list, ArrayList abi) {
        int esimene = (int) list.get(0);
        ArrayList<Integer> esimene_osa = new ArrayList<>();
        ArrayList<Integer> teine_osa = new ArrayList<>();

        //panem vasakusse elemendid, mis on vähem kui esimene element ja paremasse suuremad elemendid
        for (int i = 1; i < list.size(); i++) {
            if ((int) list.get(i) <= esimene) {
                esimene_osa.add((int) list.get(i));
            }
            if ((int) list.get(i) > esimene) {
                teine_osa.add((int) list.get(i));
            }
        }
        esimene_osa.add(esimene);

        // kontrollib, et ei olnud tühjaid liste ja paneb elemendid [väike arv -> suur arv]
        if (esimene_osa.size() > 0 && teine_osa.size() != 0) {
            kiir(esimene_osa, abi);
        } else {
            for (int i = 0; i < esimene_osa.size(); i++) {
                abi.add(esimene_osa.get(i));
            }
            if (teine_osa.size() == 1) {
                for (int i = 0; i < teine_osa.size(); i++) {
                    abi.add(teine_osa.get(i));
                }
            }
        }

        if (teine_osa.size() > 0) {
            kiir(teine_osa, abi);
        }
        else {
            for (int i = 0; i < teine_osa.size(); i++) {
                abi.add(teine_osa.get(i));
            }
        }
    }

    // mõõdab kui palju aega läheb aeglasele meetodile
    static double aeg_kiir(ArrayList list, ArrayList abi) {
        long t0 = System.nanoTime();
        kiir(list, abi);
        long t1 = System.nanoTime();
        double t2 = (t1 - t0) / 1000000.0;
        return t2;
    }

    // seda me tegime klassis
    static void nullSort(int[] a) {
        boolean oliVahetus = true;
        while (oliVahetus) {
            oliVahetus = false;
            for (int i = 1; i < a.length; i++) {
                if (a[i - 1] > a[i]) {
                    int x = a[i - 1];
                    a[i - 1] = a[i];
                    a[i] = x;
                    oliVahetus = true;
                }
            }
        }
    }

    // mõõdab kui palju aega läheb aeglasele meetodile
    static double aeg_aeglane(int[] a) {
        long t01 = System.nanoTime();
        nullSort(a);
        long t11 = System.nanoTime();
        double t21 = (t11 - t01) / 1000000.0;
        return t21;

    }

    // tehakse uus list kus on juhuslik arv elemente
    static ArrayList list_maker(int length){
        ArrayList<Integer> maked_list = new ArrayList<>();
        for (int i = 0; i<length; i++){
            int arv = (int) (-1000 + Math.random()*2000);
            maked_list.add(arv);
        }
        return maked_list;
    }

    // Arraylistist tehakse int[] massiiv
    static int[] list_to_list(int len){
        int[] inti = new int[len];
        for(int i=0; i<len; i++){
            inti[i] = (int) list_maker(len).get(i);
        }
        return inti;
    }



    // peameetod, kus antakse muutujjatele väärtused ja kutsutakse meetodid aja mõõtmiseks
    public static ArrayList<Double> main(int ab) {
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(31, 69, 12, 16, 70, 31, 16, 69, 50));
        ArrayList<Integer> abi = new ArrayList<>();
        double kiirus = aeg_kiir(list, abi);
        int[] a = {31, 69, 12, 16, 70, 31, 16, 69, 50};
        double kiirus2 = aeg_aeglane(a);
        double pikkus = list.size();
        double as = ab;
        ArrayList uus = list_maker(ab);
        double pikkus3 = uus.size();
        double kiirus3 = aeg_kiir(uus, abi);
        int[] uus2 = list_to_list(ab);
        double kiirus4 = aeg_aeglane(uus2);

        // tehakse list väärtustega [kiirus №1, pikkus №1, kiirus №2, pikkus №2...]
        ArrayList<Double> lopp = new ArrayList<>(Arrays.asList(kiirus, pikkus, kiirus2, pikkus, kiirus3, as, kiirus4, as));

    return lopp;
    }
}
