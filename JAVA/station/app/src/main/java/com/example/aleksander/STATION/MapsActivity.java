package com.example.aleksander.STATION;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener, AsyncResponse, GoogleMap.OnMyLocationChangeListener {

    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    FilterMaker filterMaker = new FilterMaker();

    private GoogleMap mMap;

    private static final String TAG = "MapsActivity";
    private List<Polyline> lastRoute = new ArrayList<>();
    final Context context = this;
    HashMap<String, List<Marker>> betterFilrer = new HashMap<>();
    private HashMap<String, List<Double>> positions = new HashMap<>();  //the key is the name of the position, the list has the x and y coordinates
    ArrayList<Marker> listOfAllMarkers = new ArrayList<>();
    HashMap<String,Integer> disabledOptions = new HashMap<>();
    List<Marker> disabledMarker = new ArrayList<>();
    ArrayList<Marker> listOfComfortMarkers = new ArrayList<>();
    AsyncResponse asyncResponse = this;
    LatLng userLocation;
    ArrayList<HashMap<String, String>> listofcomfort = new ArrayList<>();
    Integer distanceProgress;
    ArrayList<Marker> specialMarkers = new ArrayList<>();
    //the amount of global variables we have is not even funny


    @Override
    protected void onCreate(Bundle savedInstanceState) {  //the onCreate function.
        super.onCreate(savedInstanceState);
        distanceProgress = 3000;
        setContentView(R.layout.activity_maps);
        userLocation = getLocation(context);
        addListenerOnSpinnerItemSelection();


        //makes the search, don't ask how =D
        PlaceAutocompleteFragment placeAutoComplete = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete);
        placeAutoComplete.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                specialMarkers.clear();
                //Log.d("Maps", "Place selected: " + place.getName());
                LatLng secondaryPositionRadiusMarkerFinderBigNameEqualsMorePoints = place.getLatLng();
                mMap.moveCamera(CameraUpdateFactory.newLatLng(secondaryPositionRadiusMarkerFinderBigNameEqualsMorePoints));
                //mMap.addMarker(new MarkerOptions().position(secondaryPositionRadiusMarkerFinderBigNameEqualsMorePoints).title((String) place.getName()));
                for(Marker m : listOfAllMarkers){
                    Double relativePositionFromSearchPositionToMarker = CalculationByDistance(secondaryPositionRadiusMarkerFinderBigNameEqualsMorePoints, m.getPosition());
                    if(relativePositionFromSearchPositionToMarker < 10000) {
                        if (!disabledMarker.contains(m)) {
                            specialMarkers.add(m);
                            m.setVisible(true);
                        }
                    }
                }
            }

            @Override
            public void onError(Status status) {
                Log.d("Maps", "An error occurred: " + status);
            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        //asking the user permission to use geo position
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mMap = googleMap;

        try {
            addStation(positions);

        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        //no markers are filtered when launching the application

        MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(this.context, R.raw.map_style);
        mMap.setMapStyle(style);

        mMap.setMyLocationEnabled(true); //allows us to get the users location
        mMap.setOnMyLocationChangeListener(this);


        //mMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null); //animates the camera in some way

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(userLocation, 15));
        mMap.setOnInfoWindowClickListener(this);
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                marker.setSnippet(marker.getSnippet().split("Distance")[0].trim()+ " " + "Distance: " + Math.round(CalculationByDistance(userLocation, marker.getPosition()))+ " m");
                marker.showInfoWindow();
                if(listOfAllMarkers.contains(marker)){
                    for(Marker m : listOfComfortMarkers)
                        m.remove();
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 16));
                    PlacesTask placesTask = new PlacesTask(mMap);
                    placesTask.delegate2 = asyncResponse;
                    placesTask.execute(String.valueOf(sbMethod(marker.getPosition())));

                    return true;
                }
                return true;
            }
        });

        filterMaker.addListenerOnButton();
        filterMaker.filtering();
        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                System.out.println(mMap.getCameraPosition().zoom);
                if(findViewById(R.id.chkRoof).isShown()){
                    Projection projection = mMap.getProjection();
                    LatLng userPosition = userLocation;
                    Point markerPoint = projection.toScreenLocation(userPosition);
                    Point targetPoint = new Point(markerPoint.x, markerPoint.y + findViewById(R.id.simpleSlidingDrawer).getHeight() / 2);
                    LatLng targetPosition = projection.fromScreenLocation(targetPoint);
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(targetPosition));
                }
                else {
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(userLocation));
                }
                return true;
            }
        });


        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                for(Marker m : listOfComfortMarkers)
                    m.remove();
                for(Polyline p : lastRoute)
                    p.remove();
                lastRoute.clear();

            }
        });

        final Handler handler = new Handler();
        final int delay = 100000; //milliseconds

        handler.postDelayed(new Runnable(){
            public void run(){
                try {
                    getMarkersBasedOnJsonArray();
                } catch (ExecutionException | InterruptedException | JSONException e) {
                    e.printStackTrace();
                }
                handler.postDelayed(this, delay);
            }
        }, delay);
    }

    public void getMarkersBasedOnJsonArray() throws ExecutionException, InterruptedException, JSONException {
        JSONArray jsonArray = new JSONArray(new SignupActivity(this, "getLastModStations.php").execute("derm").get());
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject obj = jsonArray.getJSONObject(i);
            for(Marker m : listOfAllMarkers){
                if(obj.getString("name").equals(m.getTitle())){
                    if(!betterFilrer.get("Refueling station").contains(m) && !betterFilrer.get("Parking").contains(m)) {
                        m.setIcon(BitmapDescriptorFactory.fromResource(icon(obj.getString("icon"))));
                    }
                }
            }
        }
    }



//turn GPS on
    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i(TAG, "All location settings are satisfied.");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(MapsActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            Log.i(TAG, "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });}

    //TODO adds all the required information from json, same thing, has to be at a different class or something
    private void addStation(HashMap<String, List<Double>> hashMap) throws ExecutionException, InterruptedException {
        // positions.put("Aleksandi 2;electric;paid-2;yes;Kaubamaja", Arrays.asList(58.376568, 26.728380)); //TODO DO NOT REMOVE
        betterFilrer.put("Refueling station", new ArrayList<Marker>());
        betterFilrer.put("Charging station", new ArrayList<Marker>());
        betterFilrer.put("Parking", new ArrayList<Marker>());
        betterFilrer.put("Free for 15 minutes", new ArrayList<Marker>());
        betterFilrer.put("Free for 90 minutes", new ArrayList<Marker>());
        betterFilrer.put("No free parking", new ArrayList<Marker>());
        betterFilrer.put("Free Parking", new ArrayList<Marker>());
        betterFilrer.put("Roof", new ArrayList<Marker>());
        betterFilrer.put("Security", new ArrayList<Marker>());
        for(String s : betterFilrer.keySet()){
            disabledOptions.put(s, 0);
        }
        disabledOptions.put("Distance", distanceProgress);
        try {
            System.out.println(new SignupActivity(this, "testingAgain.php").execute("derm").get());
            JSONArray jsonArray = new JSONArray(new SignupActivity(this, "testingAgain.php").execute("derm").get());
            for (int i = 0; i < jsonArray.length(); i++) {
                //getting json object from the json array
                JSONObject obj = jsonArray.getJSONObject(i);
                double x = obj.getDouble("x");
                double y = obj.getDouble("y");
                float distanceFromUserToMarker = Math.round(CalculationByDistance(userLocation, new LatLng(x, y)));
                Marker m = mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(x, y))
                        .title(obj.getString("name"))
                        .snippet(obj.getString("priceyesorno") + " Distance: " + distanceFromUserToMarker + " m")
                        .icon(BitmapDescriptorFactory.fromResource(icon(obj.getString("icon")))));
                listOfAllMarkers.add(m);
                switch (obj.getString("Type")){
                    case("Refueling station"):
                        betterFilrer.get("Refueling station").add(m);
                        m.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher));
                        break;
                    case("Charging station"):
                        betterFilrer.get("Charging station").add(m);
                        break;
                    case("Parking"):
                        betterFilrer.get("Parking").add(m);
                        m.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.parking_icon));
                        break;
                }
                switch(obj.getString("priceyesorno")){
                    case("Free parking"): betterFilrer.get("Free Parking").add(m); break;
                    case("No free parking"): betterFilrer.get("No free parking").add(m); break;
                    case("Free for 90 minutes"): betterFilrer.get("Free for 90 minutes").add(m); break;
                    case("Free for 15 minutes"): betterFilrer.get("Free for 15 minutes").add(m); break;
                }
                switch(obj.getString("roof")){
                    case("yes"): betterFilrer.get("Roof").add(m); break;
                }
                switch(obj.getString("security")){
                    case("yes"): betterFilrer.get("Security").add(m); break;
                }
                //getting the name from the json object and putting it inside string array
                hashMap.put(obj.getString("name")+";"+obj.getString("Type")+";"+obj.getString("price")+";"+obj.getString("priceyesorno")+";"+obj.getString("roof")+";"+obj.getString("security"), Arrays.asList(Double.valueOf(obj.getString("x")), Double.valueOf(obj.getString("y"))));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int icon(String colorOfMarker){
        switch(colorOfMarker.trim()){
            case("red"): return R.mipmap.ic_red;
            case("green"): return R.mipmap.ic_green;
            case("yellow"): return R.mipmap.ic_yellow;
            default: return R.drawable.ic_action_name;
        }
    }

    @Override
    public void onInfoWindowClick(final Marker marker) {

        final Dialog dialog = new Dialog(context);

        dialog.setContentView(R.layout.custom);
        dialog.setTitle(marker.getTitle());
        //additional information when clicking the description of a marker
        for (String s : positions.keySet()) {
            String[] values = s.split(";");
            String name = values[0];
            String fuel = values[1];
            String price = values[2];
            String priceyesorno = values[3];
            String roof = values[4];
            String security = values[5];
            System.out.println(s);
            if (name.equals(marker.getTitle())) {
                TextView text_fuel = (TextView) dialog.findViewById(R.id.fuel);
                text_fuel.setText("Type: " + fuel);

                TextView text_price = (TextView) dialog.findViewById(R.id.price);
                text_price.setText(priceyesorno);

                TextView price_price = (TextView) dialog.findViewById(R.id.charging);
                price_price.setText("Price per hour: " + price + " euro");


                TextView near = (TextView) dialog.findViewById(R.id.near);
                near.setText("Roof: " + roof);

                TextView text_security = (TextView) dialog.findViewById(R.id.security);
                text_security.setText("Security: " + security);


                final ListView listView = (ListView) dialog.findViewById(R.id.listview);
                ArrayList<String> places = new ArrayList<>();


                for(HashMap<String, String> map : listofcomfort){

                    String placeName = map.get("place_name");
                    String address = map.get("vicinity");
                    String all = placeName+" : "+ address;
                    places.add(all);
                }
                //Create an adapter for the listView and add the ArrayList to the adapter.
                listView.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, places));
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        for (Marker marker : listOfComfortMarkers){
                            if (marker.getTitle().equals(parent.getAdapter().getItem(position).toString())){
                                marker.showInfoWindow();
                                mMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
                                dialog.dismiss();
                                break;
                            }
                        }
                    }

            });
                break;
        }
        }

        Button closeButton = (Button) dialog.findViewById(R.id.close);
        closeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button navigationButton = (Button) dialog.findViewById(R.id.navigaton);
        navigationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LatLng origin = userLocation;
                LatLng dest = marker.getPosition();
                new SignupActivity(context, "addADriver.php").execute(marker.getTitle(), "1");
                try {
                    boolean checker = true;
                    if(!betterFilrer.get("Refueling station").contains(marker) && !betterFilrer.get("Parking").contains(marker)){
                        marker.setIcon(BitmapDescriptorFactory.fromResource(icon(new SignupActivity(context, "getDriverAmount.php").execute(marker.getTitle()).get())));
                        final Handler handler = new Handler();

                        final Runnable mRunnable = new Runnable() {
                            @Override
                            public void run(){
                                new SignupActivity(context, "addADriver.php").execute(marker.getTitle(), "-1");
                            }
                        };
                        if(checker){
                            handler.postDelayed(mRunnable, 27000000);
                            checker = false;
                        }
                    }
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }

                // Getting URL to the Google Directions API
                String url = getDirectionsUrl(origin, dest);

                DownloadTask downloadTask = new DownloadTask();

                // Start downloading json data from Google Directions API
                downloadTask.execute(url);
                dialog.dismiss();
            }
        });

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = (int) (width * 0.97);
        lp.height = (int) (height * 0.9);

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    public void onClickBtn(View v) {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

        dialog.setContentView(R.layout.legend_info);

        dialog.show();

        Button closeButton = (Button) dialog.findViewById(R.id.button4);
        closeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    //finds the position of the user
    public LatLng getLocation(Context ctx) {
        LocationManager lm = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = lm.getProviders(true);
        Location l = null;
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ignored) {}

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ignored) {}

        if(!gps_enabled && !network_enabled) {
            // notify user
            displayLocationSettingsRequest(getApplicationContext());
        }

        for (int i = providers.size() - 1; i >= 0; i--) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return new LatLng(58.376281, 26.729013);
            }
            l = lm.getLastKnownLocation(providers.get(i));
            if (l != null)
                break;
        }
        try {
            assert l != null;
            return new LatLng(l.getLatitude(), l.getLongitude());
        }
        catch(NullPointerException ignored){

        }
        return new LatLng(58.376281, 26.729013);
    }


    public StringBuilder sbMethod(LatLng latlng) {

        //use your current location here
        double mLatitude = latlng.latitude;
        double mLongitude = latlng.longitude;

        StringBuilder sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        sb.append("location=").append(mLatitude).append(",").append(mLongitude);
        sb.append("&radius=250");
        sb.append("&types=" + "restaurant");
        sb.append("&sensor=true");
        sb.append("&key=AIzaSyAcslW-6ZAzpRjYzAwdT3CZ8gLwp6rIbLc");

        Log.d("Map", "api: " + sb.toString());

        return sb;
    }



    public double CalculationByDistance(LatLng StartP, LatLng EndP) {
        int Radius = 6371;// radius of earth in Km
        double lat1 = StartP.latitude;
        double lat2 = EndP.latitude;
        double lon1 = StartP.longitude;
        double lon2 = EndP.longitude;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double valueResult = Radius * c;
        double km = valueResult / 1;
        DecimalFormat newFormat = new DecimalFormat("####");
        int kmInDec = Integer.valueOf(newFormat.format(km));
        double meter = valueResult % 1000;
        int meterInDec = Integer.valueOf(newFormat.format(meter));
        Log.i("Radius Value", "" + valueResult + "   KM  " + kmInDec
                + " Meter   " + meterInDec);

        return Radius * c * 1000;
    }


    public void addListenerOnSpinnerItemSelection() {
        Spinner spinner1 = (Spinner) findViewById(R.id.spinner1);
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String nameOfListItem = parent.getItemAtPosition(position).toString();
                if(nameOfListItem.equals("All")){
                    disabledOptions.put("Charging station", 0);
                    disabledOptions.put("Parking", 0);
                    disabledOptions.put("Refueling station", 0);
                }
                else {
                    List<String> options = Arrays.asList("Charging station","Parking","Refueling station");
                    for(String s : options){
                        if(s.equals(nameOfListItem)){
                            disabledOptions.put(nameOfListItem, 0);
                        }
                        else disabledOptions.put(s, 1);
                    }

                }
                filterMaker.applyFilterOptions();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String nameOfListItem = parent.getItemAtPosition(position).toString();
                if(nameOfListItem.equals("All")){
                    disabledOptions.put("Free Parking", 0);
                    disabledOptions.put("No free parking", 0);
                    disabledOptions.put("Free for 90 minutes", 0);
                    disabledOptions.put("Free for 15 minutes", 0);
                }
                else {
                    List<String> options = Arrays.asList("Free Parking", "No free parking", "Free for 90 minutes","Free for 15 minutes");
                    for(String s : options){
                        if(s.equals(nameOfListItem)){
                            disabledOptions.put(nameOfListItem, 0);
                        }
                        else disabledOptions.put(s, 1);
                    }
                }
                filterMaker.applyFilterOptions();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }












    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private String getDirectionsUrl(LatLng origin,LatLng dest){

        // Origin of route
        String str_origin = "origin="+origin.latitude+","+origin.longitude;

        // Destination of route
        String str_dest = "destination="+dest.latitude+","+dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;

        return url;
    }


    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while( ( line = br.readLine()) != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
            Log.d("Exception", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }



    @Override
    public void onMyLocationChange(Location location) {
        for(Marker m : listOfAllMarkers){
            String distanceFromUser = m.getSnippet().split("Distance: | m$")[1].trim();
            if(Double.valueOf(distanceFromUser) > distanceProgress && !specialMarkers.contains(m))
                m.setVisible(false);
            else {
                if(!disabledMarker.contains(m))
                    m.setVisible(true);
            }
        }

    }

    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try{
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            if(!lastRoute.isEmpty()){
                lastRoute.get(0).remove();
                lastRoute.clear();
            }
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            // Traversing through all the routes
            for(int i=0;i<result.size();i++){
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for(int j=0;j<path.size();j++){
                    HashMap<String,String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(10);
                lineOptions.color(Color.RED);
            }

            // Drawing polyline in the Google Map for the i-th route
            Polyline polyline = mMap.addPolyline(lineOptions);
            lastRoute.add(polyline);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        return true;
    }







    @Override
    public void processFinish(List<Marker> listOfMarker, List<HashMap<String, String>> list) {
        listOfComfortMarkers.addAll(listOfMarker);
        listofcomfort.clear();
        listofcomfort.addAll(list);

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private class FilterMaker{
        void filtering(){
            View view = findViewById(R.id.content);
            view.setBackgroundColor(Color.WHITE);
            //simpleSlidingDrawer.setBackgroundColor(Color.WHITE);
        }

        void addListenerOnButton() {

            CheckBox chkRoof = (CheckBox) findViewById(R.id.chkRoof);
            CheckBox chkSecure = (CheckBox) findViewById(R.id.chkSecure);
            final TextView distanceView = (TextView) findViewById(R.id.DistanceFilter);

            SeekBar chkDistance = (SeekBar) findViewById(R.id.chkDistance);

            distanceView.setText("50000");
            for(Marker m : listOfAllMarkers){
                System.out.println(m.getSnippet());
                String distanceFromUser = m.getSnippet().split("Distance: | m$")[1].trim();
                if(Double.valueOf(distanceFromUser) > distanceProgress)
                    m.setVisible(false);
                else
                    m.setVisible(true);
            }
            chkDistance.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {


                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    distanceView.setText(String.valueOf(progress));
                    distanceProgress = progress;
                    for(Marker m : listOfAllMarkers){
                        String distanceFromUser = m.getSnippet().split("Distance: | m$")[1].trim();
                        if(Double.valueOf(distanceFromUser) > distanceProgress)
                            m.setVisible(false);
                        else {
                            if(!disabledMarker.contains(m))
                                m.setVisible(true);
                        }
                    }
                    for(Marker m: listOfComfortMarkers)
                        m.setVisible(false);
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });


            chkSecure.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (((CheckBox) v).isChecked()) {
                        disabledOptions.put("Security", 0);
                        applyFilterOptions();
                    }
                    else{
                        disabledOptions.put("Security", 1);
                        applyFilterOptions();
                    }
                }
            });
            chkRoof.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (((CheckBox) v).isChecked()) {
                        disabledOptions.put("Roof", 0);
                        applyFilterOptions();
                    }
                    else{
                        disabledOptions.put("Roof", 1);
                        applyFilterOptions();
                    }
                }
            });


        }

        private void applyFilterOptions(){
            disabledMarker.clear();
            for(String disabledOption : disabledOptions.keySet()){
                System.out.println(disabledOption);
                if(disabledOption.equals("Distance")){
                    continue;
                }
                if(disabledOptions.get(disabledOption) != 0){
                    for(Marker m : betterFilrer.get(disabledOption)){
                        disabledMarker.add(m);
                        m.setVisible(false);
                    }
                }
            }
            for(Marker m : listOfAllMarkers){
                if(!disabledMarker.contains(m))
                    m.setVisible(true);
            }
        }
    }
}
