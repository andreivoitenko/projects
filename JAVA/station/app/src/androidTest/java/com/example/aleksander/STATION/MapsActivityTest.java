package com.example.aleksander.STATION;

import android.support.test.rule.ActivityTestRule;
import android.view.View;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Elisaveta on 11/11/2017.
 */
public class MapsActivityTest {
    @Rule
    public ActivityTestRule<MapsActivity> mActivityTestRule = new ActivityTestRule<MapsActivity>(MapsActivity.class);
    private MapsActivity mActivity = null;

    @Before
    public void setUp() throws Exception {
        mActivity = mActivityTestRule.getActivity();
    }
    @Test
    public void testLaunch()
    {
        View view = mActivity.findViewById(R.id.ComfortFilter);
        assertNotNull(view);

    }

    @After
    public void tearDown() throws Exception {
        mActivity=null;

    }


}