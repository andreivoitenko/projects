package com.example.aleksander.STATION;

import android.support.test.rule.ActivityTestRule;

import com.google.android.gms.maps.model.Marker;

import org.junit.Rule;
import org.junit.Test;

import java.util.List;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.swipeDown;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.assertEquals;


public class FilterMakerTest {

    @Rule
    public ActivityTestRule<MapsActivity> mActivityRule = new ActivityTestRule<>(
            MapsActivity.class);


    @Test
    public void testSecurityFiltering() throws Exception {
        final MapsActivity activity = mActivityRule.getActivity();

        onView(withId(R.id.handle)).perform(click());
        onView(withId(R.id.chkSecure)).perform(click());
        onView(withId(R.id.handle)).perform(swipeDown());
        final List<Marker> endMarkers = activity.betterFilrer.get("Security");
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for(Marker m : endMarkers){
                    assertEquals(false, m.isVisible());
                }
            }
        });
    }

    @Test
    public void testRoofedFiltering() throws Exception {
        final MapsActivity activity = mActivityRule.getActivity();

        onView(withId(R.id.handle)).perform(click());
        onView(withId(R.id.chkRoof)).perform(click());
        onView(withId(R.id.handle)).perform(swipeDown());
        final List<Marker> endMarkers = activity.betterFilrer.get("Roof");
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for(Marker m : endMarkers){
                    assertEquals(false, m.isVisible());
                }
            }
        });
    }

    @Test
    public void testStationTypeFiltering1() throws Exception {
        final MapsActivity activity = mActivityRule.getActivity();
        onView(withId(R.id.handle)).perform(click());
        onView(withId(R.id.spinner1)).perform(click());
        onView(withText("Charging station")).perform(click());
        onView(withId(R.id.handle)).perform(swipeDown());
        final List<Marker> allMarkers = activity.listOfAllMarkers;
        final List<Marker> endMarkers = activity.betterFilrer.get("Charging station");
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for(Marker m : allMarkers){
                    if(endMarkers.contains(m)){
                        assertEquals(true, m.isVisible());
                    }
                    else{
                        assertEquals(false, m.isVisible());
                    }
                }
            }
        });
    }

    @Test
    public void testStationTypeFiltering2() throws Exception {
        final MapsActivity activity = mActivityRule.getActivity();
        onView(withId(R.id.handle)).perform(click());
        onView(withId(R.id.spinner1)).perform(click());
        onView(withText("Refueling station")).perform(click());
        onView(withId(R.id.handle)).perform(swipeDown());
        final List<Marker> allMarkers = activity.listOfAllMarkers;
        final List<Marker> endMarkers = activity.betterFilrer.get("Refueling station");
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for(Marker m : allMarkers){
                    if(endMarkers.contains(m)){
                        assertEquals(true, m.isVisible());
                    }
                    else{
                        assertEquals(false, m.isVisible());
                    }
                }
            }
        });
    }

    @Test
    public void testStationTypeFiltering3() throws Exception {
        final MapsActivity activity = mActivityRule.getActivity();
        onView(withId(R.id.handle)).perform(click());
        onView(withId(R.id.spinner1)).perform(click());
        onView(withText("Parking")).perform(click());
        onView(withId(R.id.handle)).perform(swipeDown());
        final List<Marker> allMarkers = activity.listOfAllMarkers;
        final List<Marker> endMarkers = activity.betterFilrer.get("Parking");
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for(Marker m : allMarkers){
                    if(endMarkers.contains(m)){
                        assertEquals(true, m.isVisible());
                    }
                    else{
                        assertEquals(false, m.isVisible());
                    }
                }
            }
        });
    }

    @Test
    public void testStationTypeFiltering4() throws Exception {
        final MapsActivity activity = mActivityRule.getActivity();
        onView(withId(R.id.handle)).perform(click());
        onView(withId(R.id.spinner1)).perform(click());
        onView(withText("Parking")).perform(click());
        onView(withId(R.id.handle)).perform(swipeDown());
        final List<Marker> allMarkers = activity.listOfAllMarkers;
        final List<Marker> endMarkers = activity.betterFilrer.get("Parking");
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for(Marker m : allMarkers){
                    if(endMarkers.contains(m)){
                        assertEquals(true, m.isVisible());
                    }
                    else{
                        assertEquals(false, m.isVisible());
                    }
                }
            }
        });
        onView(withId(R.id.handle)).perform(click());
        onView(withId(R.id.spinner1)).perform(click());
        onView(withText("All")).perform(click());
        onView(withId(R.id.handle)).perform(swipeDown());
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for(Marker m : allMarkers){
                    assertEquals(true, m.isVisible());
                }
            }
        });
    }

    @Test
    public void testPriceTypeFiltering1() throws Exception {
        final MapsActivity activity = mActivityRule.getActivity();
        onView(withId(R.id.handle)).perform(click());
        onView(withId(R.id.spinner)).perform(click());
        onView(withText("Free Parking")).perform(click());
        onView(withId(R.id.handle)).perform(swipeDown());
        final List<Marker> allMarkers = activity.listOfAllMarkers;
        final List<Marker> endMarkers = activity.betterFilrer.get("Free Parking");
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for(Marker m : allMarkers){
                    if(endMarkers.contains(m)){
                        assertEquals(true, m.isVisible());
                    }
                    else{
                        assertEquals(false, m.isVisible());
                    }
                }
            }
        });
    }

    @Test
    public void testPriceTypeFiltering2() throws Exception {
        final MapsActivity activity = mActivityRule.getActivity();
        onView(withId(R.id.handle)).perform(click());
        onView(withId(R.id.spinner)).perform(click());
        onView(withText("No free parking")).perform(click());
        onView(withId(R.id.handle)).perform(swipeDown());
        final List<Marker> allMarkers = activity.listOfAllMarkers;
        final List<Marker> endMarkers = activity.betterFilrer.get("No free parking");
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for(Marker m : allMarkers){
                    if(endMarkers.contains(m)){
                        assertEquals(true, m.isVisible());
                    }
                    else{
                        assertEquals(false, m.isVisible());
                    }
                }
            }
        });
    }

    @Test
    public void testPriceTypeFiltering3() throws Exception {
        final MapsActivity activity = mActivityRule.getActivity();
        onView(withId(R.id.handle)).perform(click());
        onView(withId(R.id.spinner)).perform(click());
        onView(withText("Free for 90 minutes")).perform(click());
        onView(withId(R.id.handle)).perform(swipeDown());
        final List<Marker> allMarkers = activity.listOfAllMarkers;
        final List<Marker> endMarkers = activity.betterFilrer.get("Free for 90 minutes");
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for(Marker m : allMarkers){
                    if(endMarkers.contains(m)){
                        assertEquals(true, m.isVisible());
                    }
                    else{
                        assertEquals(false, m.isVisible());
                    }
                }
            }
        });
    }

    @Test
    public void testPriceTypeFiltering4() throws Exception {
        final MapsActivity activity = mActivityRule.getActivity();
        onView(withId(R.id.handle)).perform(click());
        onView(withId(R.id.spinner)).perform(click());
        onView(withText("Free for 15 minutes")).perform(click());
        onView(withId(R.id.handle)).perform(swipeDown());
        final List<Marker> allMarkers = activity.listOfAllMarkers;
        final List<Marker> endMarkers = activity.betterFilrer.get("Free for 15 minutes");
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for(Marker m : allMarkers){
                    if(endMarkers.contains(m)){
                        assertEquals(true, m.isVisible());
                    }
                    else{
                        assertEquals(false, m.isVisible());
                    }
                }
            }
        });
    }

    @Test
    public void testPriceTypeFiltering5() throws Exception {
        final MapsActivity activity = mActivityRule.getActivity();
        onView(withId(R.id.handle)).perform(click());
        onView(withId(R.id.spinner)).perform(click());
        onView(withText("Free for 15 minutes")).perform(click());
        onView(withId(R.id.handle)).perform(swipeDown());
        final List<Marker> allMarkers = activity.listOfAllMarkers;
        final List<Marker> endMarkers = activity.betterFilrer.get("Free for 15 minutes");
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for(Marker m : allMarkers){
                    if(endMarkers.contains(m)){
                        assertEquals(true, m.isVisible());
                    }
                    else{
                        assertEquals(false, m.isVisible());
                    }
                }
            }
        });

        onView(withId(R.id.handle)).perform(click());
        onView(withId(R.id.spinner)).perform(click());
        onView(withText("All")).perform(click());
        onView(withId(R.id.handle)).perform(swipeDown());
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for(Marker m : allMarkers){
                    assertEquals(true, m.isVisible());
                }
            }
        });
    }

    @Test
    public void testDistance() throws Exception {
        final MapsActivity activity = mActivityRule.getActivity();
        onView(withId(R.id.handle)).perform(click());
        onView(withId(R.id.chkDistance)).perform(swipeLeft());

        final List<Marker> allMarkers = activity.listOfAllMarkers;
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for(Marker m : allMarkers){ //TODO no one is gonna see this anyway, but this should be changed
                    if(activity.CalculationByDistance(activity.userLocation, m.getPosition())<activity.distanceProgress)
                        assertEquals(true, m.isVisible());

                    else{
                        assertEquals(false, m.isVisible());
                    }
                }
            }
        });
    }

}