package com.example.aleksander.STATION;

import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiSelector;
import android.view.View;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import org.junit.Rule;
import org.junit.Test;

import java.util.List;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertEquals;


public class SearchLocationTest {

    @Rule
    public ActivityTestRule<MapsActivity> mActivityRule = new ActivityTestRule<>(
            MapsActivity.class);
    private MapsActivity mActivity = null;
    UiDevice mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

    @Test
    public void testSearchLocation() throws Exception {
        final MapsActivity activity = mActivityRule.getActivity();

        onView(withId(R.id.place_autocomplete)).perform(click());
        UiObject insertText = mDevice.findObject(new UiSelector().focused(true));
        insertText.setText("Soola 8");
        UiObject choice = mDevice.findObject(new UiSelector().textContains("Tartu"));
        choice.click();
        final List<Marker> endMarkers = activity.disabledMarker;
        final List<Marker> radiusMarkers = activity.specialMarkers;
        final LatLng soola8 = new LatLng(58.378857, 26.730971);
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for(Marker m : endMarkers){
                    Double dist = activity.CalculationByDistance(soola8, m.getPosition());
                    if(dist < 10000){
                        assertEquals(true, radiusMarkers.contains(m));
                    }
                }
            }
        });
    }

}
