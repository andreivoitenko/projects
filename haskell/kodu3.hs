module Kt3 where
    
    import Test.QuickCheck
    import Data.Char
    
    import Prelude hiding (any)
    
    -- 1. (1/5) Kirjuta funktsioon reverse', kasutades foldl-i.
    reverse' :: [a] -> [a]
    reverse' = foldl (#) a
      where
        a = []
        x # y = y : x
    
    
    reverseProp :: [Int] -> Bool
    reverseProp xs = reverse xs == reverse' xs
    
    
    -- 2. (1/5) Kirjuta funktsioon remSpaces', kasutades foldr-i.
    remSpaces :: String -> String
    remSpaces [] = []
    remSpaces (x:xs)
      | isSpace x = remSpaces xs
      | otherwise = x : remSpaces xs
    
    remSpaces' :: String -> String
    remSpaces' = foldr (#) a
      where
        a = []
        x # y = if isSpace x then y else x:y
    
    
    -- Seda ülesannet kindlasti testida ja lahendust vajadusel parandada.
    remSpacesProp :: String -> Bool
    remSpacesProp xs = remSpaces xs == remSpaces' xs
    
    
    -- 3. (1/5)
    -- Kasutades foldr-i, kirjuta funktsioon any, mis võtab argumendiks predikaadi ja listi.
    -- Tagasta True ainult siis, kui leidub listi elemendt, mis rahuldab predikaati.
    -- (Vihje: || on disjunktsiooni operaator)
    -- Näiteks:
    --   any isUpper "Tere!" == True   -- kuna isUpper 'T' == True
    --   any isUpper "tere!" == False
    --   ("TERE" == ['T','E','R','E'])
    any :: (a -> Bool) -> [a] -> Bool
    any f xs = foldr (\x acc -> f x || acc) False xs
    
    
    -- 4. (1/5) Kirjuta funktsioon allEqual, mis kontrollib, 
    -- kas täisarvude listi kõik elemendid on võrdsed.
    allEqual :: [Int] -> Bool
    allEqual xs = and $ map f xs
        where
          f 1 = True
          f _ = False
    
    
    
    -- 5. (1/5) Kasutades funktsiooni foldr, kirjuta unzip' funktsioon.
    -- Unzip' jagab paaride listi kaheks eraldi listiks.
    -- Näiteks:  unzip' [(1,'x'), (4,'1'), (2,'p')] == ([1, 4, 2], ['x', '1', 'p'])
    -- Näiteks:  unzip' [] == ([], [])
    --
    unzip' :: [(x, y)] -> ([x], [y])
    unzip' = foldr f b
      where
       b = undefined 
       f = undefined 
    
    