module Kt5 where
    
    data Raamat = R {
        pealkiri :: String,                  -- pealkiri
        autor    :: [([String],[String])],   -- autorite nimed (eesnimede_list, perenimede_list)
        aasta    :: Int,                     -- ilmumise aasta
        seeria   :: [Raamat]                 -- kõik sama seeria raamatud (ka raamat ise)
    } 
    
    -- Nimi võib olla näiteks: (["C.", "S."], ["Lewis"]) või (["Sacha", "Noam"], ["Baron", "Cohen"])
    
    -- 1)  (1/10) Vastavalt Raamat definitsioonile, kirjutada kirjed Sõrmuste Isanda triloogia kohta
    theFellowshipOfTheRing :: Raamat
    theFellowshipOfTheRing = R {
        pealkiri = "the Fellowship Of The Ring",
        autor = [(["J.", "R.", "R."],["Tolkien"])],
        aasta = 1954,
        seeria = [theFellowshipOfTheRing, theTwoTowers, theReturnOfTheKing]
    }

    theTwoTowers :: Raamat
    theTwoTowers = R{
        pealkiri = "the Two Towers",
        autor = [(["J.", "R.", "R."],["Tolkien"])],
        aasta = 1954,
        seeria = [theFellowshipOfTheRing, theTwoTowers, theReturnOfTheKing]
    }

    theReturnOfTheKing :: Raamat
    theReturnOfTheKing = R{
        pealkiri = "the Return Of The King",
        autor = [(["J.", "R.", "R."],["Tolkien"])],
        aasta = 1955,
        seeria = [theFellowshipOfTheRing, theTwoTowers, theReturnOfTheKing]
    }
    
    
    -- 2)  (1/5) Kirjutada funktsioon, mis saades argumendiks raamatu, 
    -- arvutab selle raamatu seeria kõikide autorite nimede listi (ühekordselt).
    --
    -- *Kt5> raamatSeeriaAutorid theReturnOfTheKing
    -- ["J. R. R. Tolkien"]  
    raamatSeeriaAutorid :: Raamat -> [String]
    raamatSeeriaAutorid = undefined
    
    
    
    -- 3.1)  (1/5) Kirjutada protseduur loePaaris, mis kirjutab "Sisesta paarisarv:" eraldi reale, siis 
    -- loeb käsurealt (kasutades funktsiooni getLine) sõne, üritab teha reads funktsiooniga
    -- arvuks. Kui arvuks tegemine ebaõnnestub või sisestatakse paaritu arv, tuleb trükkida
    -- sõne "Viga!" ja alustada protseduuri algusest. Tagastada saadud paarisarv.
    
    -- *Kt5> loePaaris 
    -- Sisesta paarisarv:
    -- 3
    -- Viga!
    -- Sisesta paarisarv:
    -- 3
    -- Viga!
    -- Sisesta paarisarv:
    -- 4
    -- 4
    
    loePaaris :: IO Int
    loePaaris = do
        putStrLn "Sisesta paarisarv:"
        s <- getLine
        case reads s :: [(Int, String)] of
            [(n, _)] -> if n `mod` 2 == 0 then return n else do 
                                                            putStrLn "Viga!" 
                                                            loePaaris
                    
    -- 3.2) (1/5) Kirjutada protseduur loeArvud, mis loeb paarisarve (kasutades loePaaris) seni 
    -- kuni sisestatakse null. Tagastada list sisestatud paarisarvudest.
    
    -- *Kt5> loeArvud 
    -- Sisesta paarisarv:
    -- 2
    -- Sisesta paarisarv:
    -- 4
    -- Sisesta paarisarv:
    -- 0
    -- [2,4]
    loeArvud :: IO [Int]
    loeArvud = do
        x <- loePaaris
        if x == 0 then return [] else (do  
                                a <- loeArvud 
                                return (x:a))

    
    
    -- main = do
    --     x <- loeArvud
    --     print x
    --     return ()
    
    class Teos x where
        t_pealkiri :: x -> Maybe String
        t_autor    :: x -> [([String],[String])]
        t_aasta    :: x -> Maybe Int
    
    instance Teos Raamat where
        t_pealkiri  = Just . pealkiri  
        t_autor     = autor
        t_aasta     = Just . aasta
    
    -- 4) (1/5) Kirjutada funktsioon, mis genereerib teose autori perenimedest ja teose ilmumise aastast
    -- lühikese viiteidentifikaatori. Sõne koosneb kuni kolme autori perenimede kuni kolm esimest tähest
    -- ja ilmumisaasta kahest viimasest numbrist.
    -- Näiteks:
    --
    -- Jaak Vilo, Alvis Brazma, Inge Jonassen, Alan J. Robinson, and Esko Ukkonen. "Mining for 
    -- putative regulatory elements in the yeast genome using gene expression data." In Ismb, 
    -- vol. 2000, pp. 384-394. 2000.
    --
    -- puhul tuleks see "VilBraJon00" ja
    --
    -- Tarmo Uustalu, and Varmo Vene. "The essence of dataflow programming." In Central European 
    -- Functional Programming School, pp. 135-167. Springer Berlin Heidelberg, 2005.
    -- 
    -- puhul "UusVen05"
    viide :: Teos x => x -> String 
    viide = undefined
    
    
    -- 5) (1/10) Kirjutada andmestruktuurile Raamat instants Show Raamat, nii et tulemus vastab mustrile 
    -- "Raamat: '<pealkiri>'". St.
    --
    -- *Kt5> show theFellowshipOfTheRing                                                                           
    -- "Raamat: 'The Fellowship of the Ring'" 
    
    instance Show Raamat where
        show raamat = "Raamat: " ++ pealkiri raamat
    
    