module Kt2 where
    
    -- NB! Kodutöö ülesanded tuleb lahendada lihtrekursiooniga, 
    -- mitte standardfunktsioone rakendades 
    
    -- 1. ristsumma leidmine positiivsest täisarvust (2/10 punkti)
    -- Näiteks:
    -- ristSumma 1234 ==> 10
    -- ristSumma 8    ==> 8
    -- ristSumma 11   ==> 2
    toList :: Integer -> [Integer] -- internetist
    toList 0 = []
    toList x = toList (x `div` 10) ++ [x `mod` 10]

    summa :: [Integer] -> Integer
    summa (x:xs) = if length (xs) > 0 then x + summa xs else x

    ristSumma :: Integer -> Integer
    ristSumma x
        | x == 0 = 0
        | otherwise = summa (toList x)
    
    
    -- 2. otsi funktsioon (2/10 punkt)
    -- Tagastab True, kui arv leidub listis, muidu False.
    -- Näiteks:
    -- otsi 3 [1,2,3,4,5,6] ==> True
    -- otsi 8 [1,2,3,4,5,6] ==> False
    otsi :: Integer -> [Integer] -> Bool
    otsi n [] = False
    otsi n (x:xs)
            | x == n = True
            | length xs == 0 = False
            | otherwise = otsi n xs
            
    
    
    -- 3. dropLast eemaldab viimase elemendi (2/10 punkt)
    -- Näiteks:
    -- dropLast []        ==> []
    -- dropLast [1,2,3]   ==> [1,2]
    -- dropLast [3,2,1,0] ==> [3,2,1]
    dropLast :: [Integer] -> [Integer]
    dropLast [] = []
    dropLast [x] = []
    dropLast (x : xs)
                | length xs < 1 = [] 
                | otherwise = x : dropLast (xs)
                

                
    
    
    -- 4. Tähe lisamine sõnesse.  (2/10 punkti)
    -- Kusjuures nulli või negatiivse indeksiga pannakse täht algusse, 
    -- suurema indeksiga, kui sõne pikkus, lõppu.
    -- Näiteks:
    -- lisa (-1) 'a' "xyz" ==> "axyz"
    -- lisa 1    'a' "xyz" ==> "xayz"
    -- lisa 3    'a' "xyz" ==> "xyza"
    -- lisa 2    'a' "xyz" ==> "xyaz"
    -- lisa 400  'a' "xyz" ==> "xyza"
    lisa :: Int -> Char -> [Char] -> [Char]
    lisa n x "" = [x]
    lisa n x (t:ys)
            | length (t:ys) < n = t : ys ++ [x]
            | n < 1 = x:t:ys
            | n == 1 = t:x:ys 
            | otherwise = t: lisa (n-1) x (ys)
    
    
    -- 5. Polünoomi arvutamine kohal x (2/10 punkti)
    -- iga listi element (a,n) tähistab polynoomi liidetavat a*x^n
    -- Näiteks: [(4.0,2),(1.0,1),(30.0,0)] tähendab 4*x^2 + x + 30
    --
    -- arvuta [(4.0,2),(1.0,1),(30.0,0)] 5 ==> 135.0
    -- arvuta [(4.0,2),(1.0,1),(30.0,0)] 2 ==> 48.0
    -- arvuta []                         5 ==> 0
    arvuta :: [(Double,Int)] -> Double -> Double
    arvuta [] x = 0
    arvuta (p:px) x = ((fst p) * (x ^ snd p)) + arvuta px x
                
    
    