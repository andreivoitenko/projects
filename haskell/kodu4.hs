module Kt4 where
    import Data.List
    import Data.Maybe
    
    
    -- 1. Listikomprehensioon (2/10)
    -- genereerida lõpmatu list, mille elemendid on kasvavas järjestuses 
    -- sellised paarisarvude kuubid, mille ruudu täisarvulisel jagamisel seitsmega
    -- saadakse jääk kaks.
    -- St. 64, 1000 jne. kuna 4^3=64,    4*4 =16,   16 `mod` 7 = 2, 
    --                       10^3=1000, 10*10=100, 100 `mod` 7 = 2.
    -- 1.a kasutades listikomprehensiooni
    arvud1 :: [Int]
    arvud1 = [a^3 | a <- [0..], even a, (a*a) `mod` 7 == 2]
    
    
    
    -- 1.b kasutades iterate, map ja filter funktsioone.
    arvud2 :: [Int]
    arvud2 = map (^3) (filter (\x -> (x*x) `mod` 7 == 2) (filter even (iterate (\x -> x+1) 1 )))

       
                
        
       

    


    
    
    -- 2. (1/10) Kirjutada foldl või foldr-i abil list, mis eelmise ülesande listis
    -- igale elemendile ühe juurde liidab. Esmalt provida mõlemat, siis jätta alles 
    -- see,  mis töötab.
    arvud3 :: [Int]
    arvud3 = foldr f [] arvud1 
        where f x y = x+1:y
    
    
    
    data BinTree a = Leaf (Maybe a) | Node (BinTree a) a (BinTree a)
        deriving (Eq, Show)
    --
    --      Node
    --     / |  \
    --  Leaf 2   Leaf
    --   |      /  | \         =  Node (Leaf (Just 1)) 2 (Node (Leaf (Just 3)) 
    -- Just  Leaf  3  L eaf                       4 (Leaf (Just 5)))
    --   |     |        |
    --   1     3        5
    --
    --
        
    
    -- 3.  (1/10) Teha funktsioon, mis loob listist "vasakule kaldu" oleva puu, st.
    --                       Node
    --                      /  |  \
    -- vasak [1,2,3] =   Node  1   Leaf
    --                  /  |  \      |
    --               Leaf  2  Leaf   Nothing
    --                |        |
    --               Just    Nothing
    --                |
    --                3
    --
    --                Leaf
    -- vasak [] =       |
    --               Nothing


    vasak :: [Int] -> BinTree Int
    vasak [] = Leaf Nothing
    vasak [x] = Leaf (Just x)
    vasak (x:xs) = Node (vasak xs) x (vasak [])
    
    
    -- 4. (2/10) Kirjutada funktsioon maksimaalse arvu leidmiseks kahendpuust.
    maks :: BinTree Integer -> Maybe Integer
    maks xs = maximum (puuListiks xs)
    
    puuListiks :: BinTree a -> [Maybe a]
    puuListiks (Leaf x) = [x]
    puuListiks (Node l a r) = puuListiks l ++ [Just a] ++ puuListiks r 

    puu = Node (Leaf (Just (-5))) 12 (Leaf (Just 2))



   -- Null | Yks |
    
    data Arvud   = Null | Yks | Kaks | Kolm | Neli | Viis | Kuus | Seitse | Kaheksa | Yheksa | Kymme   deriving (Eq, Enum, Show)
    data Mast    = Ruutu | Risti | Artu | Poti      deriving (Eq, Enum, Show)
    data Pilt    = Soldat | Emand | Kuningas | Ass      deriving (Eq, Enum, Show)
    
    data Kaart = N Mast Arvud | P Mast Pilt
        deriving (Eq, Show)
    
    -- 5. (2/10) Andke definitsioon standardsele 52-kaardiga kaardipakile. Kasutage listikomprehensiooni.
    kaardipakk :: [Kaart]
    kaardipakk = [c | a <- [Kaks .. Kymme], b <- [Ruutu .. Poti], c <- [N b a]] ++ [d | p <- [Soldat .. Ass], b <- [Ruutu .. Poti], d <- [P b p]]
    
    
    
    -- 6. (2/10) Kirjutada funktsioon, mis arvutiab iga kahe kaardi kohta, kas esimene on tugevam kui teine.
    -- Kaartide tugevusjärjestus on alates tugevamast: äss, kuningas, emand, poiss, 10, 9, 8, 7, 6, 5, 4, 3, 2
    -- Näiteks: Poti emand on tugevam, kui ruutu kaheksa.
    pakk = [c | a <- [Null .. Kymme], b <- [Ruutu .. Poti], c <- [N b a]] ++ [d | p <- [Soldat .. Ass], b <- [Ruutu .. Poti], d <- [P b p]]
    v6idab :: Kaart -> Kaart -> Bool
    v6idab x y = if toarv(elemIndex x pakk) > toarv(elemIndex y pakk) then (if toarv (elemIndex x pakk) < 36 && toarv(elemIndex y pakk) < 36 then (if takeArvud x == takeArvud y then False else True) else (if toarv(elemIndex x pakk) > 35 && toarv(elemIndex y pakk) > 35 then (if takePilt x == takePilt y then False else True) else (if toarv(elemIndex x pakk) > 35 && toarv(elemIndex y pakk) < 36 then True else (if toarv(elemIndex y pakk) >36 && toarv(elemIndex x pakk) < 35 then False else True )))) else False
    

    toarv :: Maybe Int -> Int
    toarv (Just x) = x

    takeArvud :: Kaart -> Arvud
    takeArvud (N _ x) =  x

    takePilt :: Kaart -> Pilt
    takePilt (P _ x) =  x