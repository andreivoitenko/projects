module Kt1 where
    
    import Data.Bits
    import Test.QuickCheck hiding ((.&.))
      
    -- implementeeri funktsioonid vastavalt fun.pdf definitsioonidele
    
    -- 1/10 punkti
    i :: Int -> Int
    i n = n
    
    
    -- 2/10 punkti
    p :: Int -> Int -> Int
    p n 0 = 1
    p n 1 = n
    p n k = n * p (n-1) (k-1)
    
    
    
    -- 2/10 punkti

    --fact :: Int -> Int
    --fact 0 = 1
    --fact n = n * fact (n-1)

    c :: Int -> Int -> Int
    c n 0 = 1 
    c n k = if k <= n-1 then (fact (n - 1) `div` (fact (k - 1) * fact ((n - 1) - (k - 1)))) + (fact (n-1) `div` (fact (k) * fact ((n-1) - (k)))) else 0
            where fact z
                    | z == 0 = 1
                    | otherwise = z * fact (z-1)
        

    -- 2/10 punkti
    d :: Int -> Int -> Int
    d x y = f x 0
        where f n z
                | n < y     = z
                | otherwise = f (n-y) (z+1)
    
    
    -- 3/10 punkti
    mydiv :: Int -> Int -> Int
    mydiv n d 
        | n < 0 = error("invalid argument")
        | d < 0 = error("invalid argument")
        | d == 0 = error("division by zero")
        | otherwise = f 0 0 (finiteBitSize (n - 1))
            where f r q i
                    | i == -1 = q
                    | 2 * r + g n i >= d = f (2 * r + g n i -d) (q + 2^i) (i-1)
                    | otherwise = f (2 * r + g n i) q (i-1)
                        where g i n = shiftR i n .&. 1
                                
    
    

    h :: [Int] -> [Int]
    h xs = map g xs ++ [3]
        where g x = x-1