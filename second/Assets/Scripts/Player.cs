﻿using UnityEngine;

public class Player : MonoBehaviour
{
    private float playerSpeed;
    public Transform gunPlace;
    private double timeStamp;
    public float playerMaxHp;
    public float playerHp;
    public float sprintSpeed;
    public Animator anim;
    public bool hasGun;
    public GameObject currentGun;
    public GameObject player;
    private float staminaMax;
    private float staminaCur;
    public int expCounter;
    System.Random rnd;
    public GameObject isthisit;
    public GameObject player_Stamina_Bar;
    public GameObject player_Health_Bar;
    public GameObject backUpWeapon;
    private Gun gun;




    void Start()
    {
        playerMaxHp = 10f;
        playerHp = playerMaxHp;
        playerSpeed = 15;
        sprintSpeed = 30;
        staminaMax = 10f;
        staminaCur = staminaMax;
        expCounter = 10; //temporary for testing
        
    }

    void FixedUpdate()
    {
        if(currentGun == null)
        {
            //currentGun = backUpWeapon;
            GameObject gun = (GameObject) Instantiate(backUpWeapon, transform.position, transform.rotation);
            gun.name = backUpWeapon.name;
        }
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.LoadLevel("Menu");
        }
        if (playerHp <= 0f)
        {
            //Destroy(gameObject);
            Application.LoadLevel("Menu");
        }
        if (expCounter >= 15)
            randomStat();

        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.S))
        {
            anim = GetComponent<Animator>();
            anim.Play("walk 0");
        }
        if (Input.GetKey(KeyCode.LeftShift) && staminaCur > 0f)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(Input.GetAxis("Horizontal") * sprintSpeed, Input.GetAxis("Vertical") * sprintSpeed);
            if (staminaCur - 1f < 0f)
            {
                staminaCur = 0f;
                sprintSpeed = 0;
                SetStaminaBarPlayer(0);
            }
            else
            {
                staminaCur -= 1f;
                SetStaminaBarPlayer(staminaCur / staminaMax);
            }
        }
        else
        {
            int counter = 0;
            if (staminaCur < staminaMax)
            {
                if (counter < 100 && staminaCur < 2f)
                {
                    counter++;
                    staminaCur += 0.1f;
                    SetStaminaBarPlayer(staminaCur / staminaMax);
                }
                else
                {
                    staminaCur += 0.1f;
                    SetStaminaBarPlayer(staminaCur / staminaMax);
                    sprintSpeed = 30;
                }
            }
                
            GetComponent<Rigidbody2D>().velocity = new Vector2(Input.GetAxis("Horizontal") * playerSpeed, Input.GetAxis("Vertical") * playerSpeed);
        }
        Look();
    }

    void Look()
    {
        Vector3 mousePos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10);
        Vector3 lookPos = Camera.main.ScreenToWorldPoint(mousePos);
        lookPos = lookPos - transform.position;
        float angle = Mathf.Atan2(lookPos.y, lookPos.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    public void SetStaminaBarPlayer(float toSet)
    {
        player_Stamina_Bar.transform.localScale = new Vector3(toSet, player_Stamina_Bar.transform.localScale.y, player_Stamina_Bar.transform.localScale.z);
    }

    public void SetHealthBarPlayer(float toSet)
    {
        player_Health_Bar.transform.localScale = new Vector3(toSet, player_Health_Bar.transform.localScale.y, player_Health_Bar.transform.localScale.z);
    }

    void randomStat()
    {
        expCounter = 10; //temporary for testing
        rnd = new System.Random();
        int[] stats = new int[4];
        stats[0] = (int)playerMaxHp;
        stats[1] = (int)staminaMax;
        stats[2] = (int)playerSpeed;
        stats[3] = (int)sprintSpeed;
        int r = rnd.Next(stats.Length);
        stats[r] += 1;

        if (playerHp < stats[0])
        {
            if (playerHp == playerMaxHp)
            {
                playerHp = stats[0];
            }
            playerMaxHp = stats[0];
            popUp("MAX HP UP");
            if (rnd.Next(100) < 50 && playerHp < playerMaxHp) // restores health with 50% chance
            {
                playerHp = playerMaxHp;
                SetHealthBarPlayer(1);
                //popUp("HP RESTORED"); // Messing up with MAX HP UP message
            }
            else
            {
                SetHealthBarPlayer(playerHp / playerMaxHp);
            }
            return;
            
        }
        else if(staminaMax < stats[1])
        {
            staminaMax = stats[1];
            popUp("MAX STAMINA UP");
            SetStaminaBarPlayer(staminaCur / staminaMax);
            return;
        }
        else if(playerSpeed < stats[2])
        {
            playerSpeed = stats[2];
            popUp("PLAYER SPEED UP");
            return;
        }
        else
        {
            sprintSpeed = stats[3];
            popUp("SPRINT SPEED UP");
            return;
        }
    }
    void popUp(string text){
        Vector3 textLocation = Camera.main.WorldToScreenPoint(transform.position);
        textLocation.x /= Screen.width + 50;
        textLocation.y /= Screen.height + 50;
        isthisit.GetComponent<GUIText>().text = text;
        GameObject statT = Instantiate(isthisit, textLocation, Quaternion.identity) as GameObject;
        Destroy(statT, 1f);
    }

    /*
        void gunSwith()
        {
            if (Input.GetKeyDown(KeyCode.Alpha1) && pistol != null)
            {
                currentGun = pistol;
            }
            if (Input.GetKeyDown(KeyCode.Alpha2) && machineGun != null)  //work in progress
            {
                currentGun = machineGun;
            }
            if (Input.GetKeyDown(KeyCode.Alpha3) && shotGun != null)
            {
                currentGun = shotGun;
            }
        }
    */

}
