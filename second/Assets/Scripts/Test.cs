﻿using UnityEngine;
using System.Collections;

public class Test : MonoBehaviour {

    private int health;
    public GameObject goodies;
    private bool cheat;

	// Use this for initialization
	void Start () {
        health = 1;
        cheat = true;
	}
	
	// Update is called once per frame
	void Update () {
	    if(health <= 0 && cheat)
        {
            for(int i = 0; i < 4; i++)
            {
                GameObject clone = (GameObject) Instantiate(goodies, transform.position, transform.rotation);
                clone.GetComponent<Rigidbody2D>().AddForce(random((clone.transform.position - GameObject.FindGameObjectWithTag("Player").transform.position))/75, ForceMode2D.Force);
                //GetComponent<Rigidbody2D>().velocity = new Vector3(diff.x * bulletSpeed, diff.y * bulletSpeed, diff.z);

                cheat = false;
            }
            Destroy(gameObject);
        }
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "PistolBullet")  //this is horrible, because this system will lead to 100 ifs (new if for every new bullet type, like SMGBullet, SHOTGUNbullet and so on)
        {
            health--;
        }

        if (other.gameObject.tag == "SniperBullet")
        {
            health -= 5;
        }
    }

    Vector3 random(Vector3 pos)
    {
        int strayFactor = 10;
        var randomNumberX = Random.Range(-strayFactor, strayFactor);
        var randomNumberY = Random.Range(-strayFactor, strayFactor);
        var randomNumberZ = Random.Range(-strayFactor, strayFactor);
        pos.x = pos.x - randomNumberX;
        pos.y = pos.y - randomNumberY;
        pos.z = pos.z - randomNumberZ;
        return pos;
    }
}
