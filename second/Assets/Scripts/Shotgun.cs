﻿using UnityEngine;
using System.Collections;

public class Shotgun : MonoBehaviour {

    public Transform firePoint;
    public GameObject bullet;
    private double cd;
    private bool canFire;
    private Gun gunScript;
    private bool enableRotation = false;
    private bool cheat = true;
    private Player player;
    private int ammo;
    private int clip = 6;

    void Start()
    {
        ammo = 24;
        cd = 1;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        gunScript = new Gun(firePoint, bullet, cd, clip);
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse0) && canFire)
        {
            gunScript.fireGun(player.player, 10);
        }
        if (enableRotation)
        {
            gameObject.transform.position = player.gunPlace.transform.position;
            gameObject.transform.rotation = player.gunPlace.transform.rotation;
            firePoint.rotation = player.gunPlace.transform.rotation;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player" && cheat)
        {
            cheat = false;
            gunScript.pickUpWeapon(gameObject);
            canFire = true;
            enableRotation = true;
            if (player.currentGun != null)
            {
                Destroy(player.currentGun);
                player.currentGun = gameObject;
                gunScript.ammo = ammo;
            }
            else
            {
                player.currentGun = gameObject;
                gunScript.ammo += ammo;
            }
        }
    }
}
