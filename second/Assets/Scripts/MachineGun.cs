﻿using UnityEngine;
using System.Collections;

public class MachineGun : MonoBehaviour
{

    public Transform firePoint;
    public GameObject bullet;
    private double cd;
    private bool canFire;
    private Gun gunScript;
    private bool enableRotation = false;
    private bool cheat = true;
    private Player player;
    private int ammo;
    private int clip;

    // Use this for initialization
    void Start()
    {
        cd = 0.1;
        ammo = 70;
        clip = 15;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        gunScript = new Gun(firePoint, bullet, cd, clip);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse0) && canFire && player.currentGun == gameObject)
        {
            gunScript.fireGun(player.player, 1);
        }
        if (enableRotation)
        {
            gameObject.transform.position = player.gunPlace.transform.position;
            gameObject.transform.rotation = player.gunPlace.transform.rotation;
            firePoint.rotation = player.gunPlace.transform.rotation;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player" && cheat)
        {
            cheat = false;
            gunScript.pickUpWeapon(gameObject);
            canFire = true;
            enableRotation = true;
            if (player.currentGun != null)
            {
                Destroy(player.currentGun);
                player.currentGun = gameObject;
                gunScript.ammo = ammo;
            }
            else
            {
                player.currentGun = gameObject;
                gunScript.ammo += ammo;
            }
        }
    }
}