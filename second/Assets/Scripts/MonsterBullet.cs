﻿using UnityEngine;
using System.Collections;

public class MonsterBullet : MonoBehaviour {

    public int bulletSpeed = 1;

    public Vector3 pos;

    private Player player;

    public GameObject player_Health_Bar;


    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        pos = new Vector3(player.transform.position.x, player.transform.position.y);


    }

    void Update()
    {
        Vector3 diff = pos - transform.position;
        diff.Normalize();
        //transform.position = transform.position + diff * bulletSpeed * Time.deltaTime;
        //Destroy(gameObject, 0.3f);
        //GetComponent<Rigidbody2D>().velocity = new Vector3(diff.x * bulletSpeed, diff.y * bulletSpeed, diff.z);
        gameObject.GetComponent<Rigidbody2D>().AddForce(diff*100);
        Destroy(gameObject, 0.5f);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        print("triggered");
        if (other.gameObject.tag == "Player")
        {
            player.playerHp = player.playerHp - 1f;
            SetHealthBarPlayer(player.playerHp / player.playerMaxHp);
            //play sound or something
        }
    }

    public void SetHealthBarPlayer(float toSet)
    {
        player_Health_Bar.transform.localScale = new Vector3(toSet, player_Health_Bar.transform.localScale.y, player_Health_Bar.transform.localScale.z);
    }
}
