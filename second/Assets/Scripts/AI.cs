﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AI : MonoBehaviour
{

    private Transform Target;
    public float speed = 2f;
    public int health = 3;
    private Player player;
    private double timeStamp;
    public double cd;
    private float aggrorange;
    private bool aggro = false;
    public Color color;
    public GameObject FloatingDamagePrefab; //v unity (net tut a v unity) naidi skript AI i zasun tuda krov vmesto etogo, dolwno rabotat navernoe =DD. Takwe ne zabud potom vsem zombikam v igre eto postavit, skirpt tak ne budet soxranjatsja
    public GameObject EXP;
    //public GameObject BloodPOsition;
    //public float AI_Cur_Health = 100f;
    public GameObject player_Health_Bar;
    public int type;
    public GameObject minions;
    public Transform firePoint;
    public GameObject monsterBullet;
    //public Text txt;


    void Start()
    {
        cd = 2;
        //EXP.GetComponent<GUIText>().text = "+5 EXP";
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        Target = player.GetComponent<Transform>();
        

    }

    

    void FixedUpdate()
    {
        if (health <= 0)
        {
            player.expCounter += 5; //instead of constant 5 each monster should have his own exp, should do it later when we start balancing things
            dieType();
        }
        if ((Target.position.x - transform.position.x <= 20 || Target.position.x - transform.position.x <= -20) && (Target.position.y - transform.position.y <= 20 || Target.position.y - transform.position.y <= -20) || aggro)
            attackPlayer();

    }

    void Look()
    {
        Vector3 playerPos = new Vector3(Target.position.x, Target.position.y, Target.position.z);
        playerPos = playerPos - transform.position;
        float angle = Mathf.Atan2(playerPos.y, playerPos.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        //Vector3 abc = new Vector3(transform.position.x, transform.position.y, -1);
        //var copy = Instantiate(sledy, abc, transform.rotation);
        //Destroy(copy, 5f);
        

    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player" && type != 3) 
        {
            if (timeStamp <= Time.time)
            {
                player.playerHp = player.playerHp - 1f;
                SetHealthBarPlayer(player.playerHp / player.playerMaxHp);
                timeStamp = Time.time + cd;
                //play sound or something
            }
        }
    }


    public void SetHealthBarPlayer(float toSet)
    {
        player_Health_Bar.transform.localScale = new Vector3(toSet, player_Health_Bar.transform.localScale.y, player_Health_Bar.transform.localScale.z);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Vector3 abi = new Vector3(transform.position.x, transform.position.y, -1);
        if (other.gameObject.tag == "PistolBullet")  //this is horrible, because this system will lead to 100 ifs (new if for every new bullet type, like SMGBullet, SHOTGUNbullet and so on)
        {
            health--;
            
            GameObject tempFloatingDamage = Instantiate(FloatingDamagePrefab, abi, transform.rotation) as GameObject;
            aggro = true;
        }

        if (other.gameObject.tag == "SniperBullet")
        {
            health -= 5;
            GameObject tempFloatingDamage = Instantiate(FloatingDamagePrefab, abi, transform.rotation) as GameObject;
            aggro = true;
        }
    }


    void attackPlayer()
    {
        GetComponent<Rigidbody2D>().MovePosition(Vector2.MoveTowards(transform.position, Target.position, speed * Time.deltaTime));
        if(type != 3)
        {
            Look();
        }
        if (type == 2)
            fireGuts();
        if (type == 3)
        {
            if (timeStamp <= Time.time && GameObject.FindGameObjectsWithTag("SpMinion").Length < 5)
            {
                Instantiate(minions, transform.position, transform.rotation);
                popUp("SPAWN"); //remove this later
                timeStamp = Time.time + cd;
                //play sound or something
            }
        }
    }

    void dieType()
    {
        if (type == 1)
        {
            Instantiate(minions, transform.position, transform.rotation);
            Instantiate(minions, transform.position, transform.rotation);
            Instantiate(minions, transform.position, transform.rotation);
            Instantiate(minions, transform.position, transform.rotation);
            Destroy(gameObject);
            popUp("BOOM");
        }
        else
        {
            Destroy(gameObject);
            popUp("+5 EXP");
        }
    }

    void fireGuts()
    {
        if (timeStamp <= Time.time)
        {
            Instantiate(monsterBullet, firePoint.position , firePoint.rotation);
            timeStamp = Time.time + cd;
        }
    }

    void popUp(string word)
    {
        EXP.GetComponent<GUIText>().text = word;
        Vector3 expLocation = Camera.main.WorldToScreenPoint(transform.position);
        expLocation.x /= Screen.width;
        expLocation.y /= Screen.height;
        GameObject tempExp = Instantiate(EXP, expLocation, Quaternion.identity) as GameObject;
        Destroy(tempExp, 0.5f);
    }
}

