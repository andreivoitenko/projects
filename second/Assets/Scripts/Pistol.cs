﻿using UnityEngine;
using System.Collections;

public class Pistol : MonoBehaviour
{

    public Transform firePoint;
    public GameObject bullet;
    private double cd;
    private bool canFire;
    private Gun gunScript;
    private bool enableRotaion = false;
    private bool cheat = true;
    private Player player;
    public AudioClip shootSound;
    private AudioSource source;
    private int ammo;
    private int clip;



    // Use this for initialization
    void Start()
    {
        ammo = 20;
        clip = 5;
        cd = 0;
        gunScript = new Gun(firePoint, bullet, cd, clip);
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        source = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0) && canFire && player.currentGun == gameObject)
        {
            source.PlayOneShot(shootSound,1);
            gunScript.fireGun(player.player, 1);
        }
        if (enableRotaion)
        {
            gameObject.transform.position = player.gunPlace.transform.position;
            gameObject.transform.rotation = player.gunPlace.transform.rotation;
            firePoint.rotation = player.gunPlace.transform.rotation;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player" && cheat)
        {
            cheat = false;
            gunScript.pickUpWeapon(gameObject);
            canFire = true;
            enableRotaion = true;
            if (player.currentGun != null)
            {
                Destroy(player.currentGun);
                player.currentGun = gameObject;
                gunScript.ammo = ammo;
            }
            else
            {
                player.currentGun = gameObject;
                gunScript.ammo += ammo;
            }
        }
    }

}