﻿using UnityEngine;
using System.Collections;
using System;

public class Gun : MonoBehaviour
{

    public Transform firePoint;
    public GameObject bullet;
    private double timeStamp;
    public double cd;
    public GameObject weapon;
    public int strayFactor;
    public int ammo;
    public int clip;
    private int maxClip;

    public Gun(Transform firePoint, GameObject bullet, double cd, int clip)
    {
        this.firePoint = firePoint;
        this.bullet = bullet;
        this.cd = cd;
        this.clip = clip;
        maxClip = clip;
    }
    public void fireGun(GameObject player, int amountOfBullets)
    {
        if (timeStamp <= Time.time && ammo != 0)
        {
            for(int i = 0; i<amountOfBullets; i++)
                Instantiate(bullet, firePoint.position, firePoint.rotation);
            timeStamp = Time.time + cd;
            gunKick(player);
            clip--;
            ammo--;
            print(ammo + " ammo");
            print(clip + " clip");
            if (clip <= 0)
            {
                if (cd >= 0.5)
                    timeStamp = Time.time + cd * 2;
                else
                    timeStamp = Time.time + 1;
                clip += maxClip;
                print("reloading");
                //need to add a reloading bar
            }
        }
    }

    public void pickUpWeapon(GameObject item)
    {
        weapon = item;
    }

    public void deleteWeapon()
    {
        Destroy(weapon);
    }

    public void gunKick(GameObject player)
    {
        Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 diff = pos - player.transform.position;
        diff.x = diff.x - 1;
        diff.y = diff.y - 1;
        diff.z = 0;
        diff.Normalize();
        player.transform.position = player.transform.position - diff;
    }

}   