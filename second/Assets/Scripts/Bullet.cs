﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

    public int bulletSpeed = 500;

    public GameObject player;  //useless i think

    public Vector3 pos;

    public int strayFactor;

    public Transform bullet;


    void Start () {
        if(transform.tag == "SniperBullet")
            strayFactor = 1;
        else
        {
            strayFactor = 4; //should be based on the weapon
        }
        pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        var randomNumberX = Random.Range(-strayFactor, strayFactor);
        var randomNumberY = Random.Range(-strayFactor, strayFactor);
        var randomNumberZ = Random.Range(-strayFactor, strayFactor);
        pos.x = pos.x - randomNumberX;
        pos.y = pos.y - randomNumberY;
        pos.z = pos.z - randomNumberZ;
    }

    void Update()
    {
        Vector3 diff = pos - transform.position;
        diff.Normalize();
        transform.position = transform.position + diff * bulletSpeed * Time.deltaTime;
        
        if (transform.tag != "SniperBullet")
            Destroy(gameObject, 0.2f);
        else
        {
            Destroy(gameObject, 0.5f);
        }
    }

    void OnTriggerEnter2D(Collider2D other)  //OnTriggerEnter2D eto trigger v Box Collidere i Circle Collidere. Tam est check box. Poetomu inogda bullets iswezajus randomno.
    {
        if(other.tag != "PistolBullet" && transform.tag != "SniperBullet")
             Destroy(gameObject);
        if (transform.tag == "SniperBullet" && other.tag != "Enemy")
            Destroy(gameObject);
        

    }
}
