﻿using UnityEngine;
using System.Collections;

public class camera : MonoBehaviour
{

    public GameObject target;


    // Use this for initialization
    void Start()
    {
        transform.position = new Vector3(target.transform.position.x, target.transform.position.y, target.transform.position.z-10);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(target.transform.position.x, target.transform.position.y, transform.position.z);
    }
}
