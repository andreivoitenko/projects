﻿using UnityEngine;
using System.Collections;

public class PeaShooter : MonoBehaviour
{

    public Transform firePoint;
    public GameObject bullet;
    private double cd;
    private bool canFire;
    private Gun gunScript;
    private bool enableRotaion = false;
    private bool cheat = true;
    private Player player;
    public AudioClip shootSound;
    private AudioSource source;
    private int ammo;
    private int clip;
    private double timeStamp;



    // Use this for initialization
    void Start()
    {
        ammo = int.MaxValue;
        clip = 6;
        cd = 0.6;
        gunScript = new Gun(firePoint, bullet, cd, clip);
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        source = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0) && canFire && player.currentGun == gameObject)
        {
            print("pressing it");
            source.PlayOneShot(shootSound, 1);
            if (timeStamp <= Time.time && ammo != 0)
            {
                for (int i = 0; i < 1; i++)
                    Instantiate(bullet, firePoint.position, firePoint.rotation);
                timeStamp = Time.time + cd;
                clip--;
                ammo--;
                print(ammo + " ammo");
                if (clip <= 0)
                {
                    timeStamp = Time.time + cd * 2;
                    clip += 6;
                    print("reloading");
                    //need to add a reloading bar
                }
            }
        }
        if (enableRotaion)
        {
            gameObject.transform.position = player.gunPlace.transform.position;
            gameObject.transform.rotation = player.gunPlace.transform.rotation;
            firePoint.rotation = player.gunPlace.transform.rotation;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Start();
        print(ammo);
        if (other.gameObject.tag == "Player" && cheat)
        {
            cheat = false;
            gunScript.pickUpWeapon(gameObject);
            canFire = true;
            enableRotaion = true;
            if (player.currentGun != null)
            {
                Destroy(player.currentGun);
                player.currentGun = gameObject;
                gunScript.ammo = ammo;
            }
            else
            {
                player.currentGun = gameObject;
                gunScript.ammo += ammo;
            }
        }
    }

}