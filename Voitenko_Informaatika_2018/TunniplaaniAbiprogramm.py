from random import randint

with open("KasitsiTunniplaan.txt") as f:
    content = f.readlines()
    lopp = ""
    for line in content:
        # juhuslikud arvud
        RandomSubjectName = randint(0, 27)
        RandomPeople = randint(7, 50)
        RandomType = randint(0, 4)
        NameRandom = randint(0, 8)

        # massivid koos nimetustega
        SubjectNames = ["Erialane inglise keel informaatika üliõpilastele","Programmeerimine", "Kõrgem matemaatika I", "Sissejuhatus informaatikasse", "Matemaatiline maailmapilt", "Arvuti arhitektuur ja riistvara I", "Objektorienteeritud programmeerimine", "Veebilehtede loomine", "Kõrgem matemaatika II", "Andmebaasid", "Graafid ja matemaatiline loogika", "Operatsioonisüsteemid", "Algoritmid ja andmestruktuurid", "Tarkvaratehnika", "Sissejuhatus majandusteooriasse", "Arvutimängude loomine ja disain", "Tarkvara testimine", "Andmeturve", "Infotehnoloogia sotsiaalsed aspektid", "Veebirakenduste loomine", "Automaadid, keeled ja translaatorid", "Kasutajaliideste kavandamine", "Tõenäosusteooria ja matemaatiline statistika", "Tarkvaraprojekt", "Programmeerimiskeeled", "Tehisintellekt", "Arvutigraafika", "Eestikeelne kommunikatsioon arvutiteaduses"]
        SubjectNamesENG = ["English for Students of Computer Science", "Computer Programming", "Calculus I", "Introduction to Informatics", "Transition to Advanced Mathematics", "Computer Architecture and Hardware I", "Object-oriented Programming" , "Creating WWW Pages", "Calculus II", "Databases", "Graphs and Mathematical Logic", "Operating Systems", "Algorithms and Data Structures", "Software Engineering", "Introduction to Economics", "Computer Game Development and Design", "Software Testing", "Computer Security", "Social Aspects of Information Technology", "Web Application Development", "Automata, Languages and Compilers", "User Interface Design", "Probability and Mathematical Statistics", "Software Project", "Programming Languages", "Artificial Intelligence", "Computer Graphics", "Communication in Estonian for Computer Science"]
        types = ["practical session", "lecture", "seminar", "exam", "test"]
        names = ["Anna Tamm", "Maria Sepp", "Tiina Mägi", "Sirje Vasiliev", "Anne Kask", "Andres Rebane", "Jüri Pärn", "Martin Koppel", "Toomas Luik"]

        # iga rea kontrollimine ning vajali informatsiooni muutmine
        if ("Aine 1" in line):
            line = line.replace("Aine 1", SubjectNames[RandomSubjectName])
            line += "\"Name_Eng\": \"" + SubjectNamesENG[RandomSubjectName] + "\",\n" + "\"Type\": \"" + types[RandomType] + "\",\n"
            lopp+=line
        elif ("Aine 2" in line):
            line = line.replace("Aine 2", SubjectNames[RandomSubjectName])
            line += "\"Name_Eng\": \"" + SubjectNamesENG[RandomSubjectName] + "\",\n" + "\"Type\": \"" + types[RandomType] + "\",\n"
            lopp+=line
        elif ("Aine 3" in line):
            line = line.replace("Aine 3", SubjectNames[RandomSubjectName])
            line += "\"Name_Eng\": \"" + SubjectNamesENG[RandomSubjectName] + "\",\n" + "\"Type\": \"" + types[RandomType] + "\",\n"
            lopp+=line
        elif ("Aine 4" in line):
            line = line.replace("Aine 4", SubjectNames[RandomSubjectName])
            line += "\"Name_Eng\": \"" + SubjectNamesENG[RandomSubjectName] + "\",\n" + "\"Type\": \"" + types[RandomType] + "\",\n"
            lopp+=line
        elif ("Aine 5" in line):
            line = line.replace("Aine 5", SubjectNames[RandomSubjectName])
            line += "\"Name_Eng\": \"" + SubjectNamesENG[RandomSubjectName] + "\",\n" + "\"Type\": \"" + types[RandomType] + "\",\n"
            lopp+=line
        elif ("\"Students\": --" in line):
            abi = "\"Students\": "+ str(RandomPeople)
            line = line.replace("\"Students\": --", abi)
            lopp+=line
        elif ("\"Teacher\": \"Name\"" in line):
            abi = "\"Teacher\": "+ "\""+str(names[NameRandom])+"\""
            line = line.replace("\"Teacher\": \"Name\"", abi)
            lopp+=line
        else:
            lopp+=line

    print(lopp)
